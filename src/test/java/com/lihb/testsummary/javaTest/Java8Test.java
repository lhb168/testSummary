package com.lihb.testsummary.javaTest;

import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import com.alibaba.fastjson.JSON;
import com.lihb.testsummary.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@SpringBootTest
class Java8Test {

    @Test
    void contextLoads() {
    }

    /**
     * 流测试
     *
     * @throws Exception 异常
     */
    @Test
    void streamTest() throws Exception {
        List<User> users = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            User temp = new User();
            temp.setLoginName("lihb"+i);
            temp.setName("lihb"+i);
            temp.setSorted(i);
            users.add(temp);
        }
        // 1、forEach
        // 可以通过 `::` 关键字来访问类的构造方法，对象方法，静态方法。
        // 在流中的使用相当于把方法当做参数传到stream内部，使stream的每个元素都传入到该方法里面执行一下
        // 下面两个方法等价
        users.stream().forEach(Java8Test::print);
        users.stream().forEach(o->Java8Test.print(o.getLoginName()));
        users.stream().forEach(item->item.setPassword("1234"));
        System.out.println(JSON.toJSON(users));
        // 2、map
        // map简单点理解就是将流中的对象转换成另一个对象
        List<String> collect = users.stream().map(item -> item.getName()).collect(Collectors.toList());
        // 可以转换后直接打印
        users.stream()
                .map(item -> item.getName().split(" "))
                .forEach(o-> {
                    for (String s : o) {
                        System.out.println(s);
                    }
                });

        // 3、filter
        users.stream().filter(item->item.getSorted()>5);

    }

    public static void print(User user) {
        String test = "姓名：" + user.getName();
        System.out.println(test);
    }

    public static void print(String test) {
        test = "登陆名：" + test;
        System.out.println(test);
    }

    /**
     * 流测试--map
     *
     * @throws Exception 异常
     */
    @Test
    void streamMapTest() throws Exception {
        List<User> users = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            User temp = new User();
            temp.setLoginName("lihb"+i);
            temp.setName("li hb"+i);
            users.add(temp);
        }

        // map简单点理解就是将流中的对象转换成另一个对象
        List<String> collect = users.stream().map(item -> item.getName()).collect(Collectors.toList());
        users.stream()
                .map(item -> item.getName().split(" "))
                .forEach(o-> {
                    for (String s : o) {
                        System.out.println(s);
                    }
                });

        ExcelWriter writer = ExcelUtil.getWriter(true);
    }
}
