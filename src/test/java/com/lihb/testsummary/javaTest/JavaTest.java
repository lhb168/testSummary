package com.lihb.testsummary.javaTest;

import com.alibaba.fastjson.JSON;
import com.gisquest.platform.common.security.sm.SM2Utils;
import com.gisquest.platform.common.utils.JsonObjectUtil;
import com.gisquest.platform.common.utils.StringUtils;
import com.gisquest.realestate.data.supervise.judicial.enums.FailEnum;
import com.gisquest.realestate.data.supervise.judicial.exception.InternalServiceException;
import com.lihb.testsummary.entity.*;
import com.lihb.testsummary.utils.RsaUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * 测试目录
 * 1、值传递测试
 *
 * @author 李海滨
 * @date 2021/09/03
 */
@SpringBootTest
@Slf4j
class JavaTest {
    /**
     * 业务状态（新建）
     */
    public static final Integer ZT_NEW = 0;

    /**
     * 目录
     */
    void directory() throws Exception {
        //1、值传递测试
        this.valuePassTest();
        //2、日志测试
        this.logTest();
        //3、异常测试
        this.throwTest();
        //

    }

    /**
     * 值传递测试
     *
     * @throws Exception 异常
     */
    @Test
    void valuePassTest() throws Exception {
        User user =new User();
        user.setName("valuePassTest");
        System.out.println("前："+ JSON.toJSONString(user));//前：{"name":"valuePassTest"}
        test1q2(user);
        System.out.println("后："+JSON.toJSONString(user));//后：{"id":11,"name":"test1q2","password":"12"}
    }

    private void test1q2(User user){
        user.setId(11L);
        user.setPassword("12");
        user.setName("test1q2");
    }

    @Test
    void logTest() throws Exception {
        try {
            List<String> test = new ArrayList<>(1);
            test.add("1");
            test.add("2");
            test.add("3");
            System.out.println(test.get(5));
        }catch (Exception e){
            log.error("测试报错1:",e);
            e.printStackTrace();
            log.error("测试报错2:"+e.getMessage());
        }
    }

    @Test
    void throwTest() throws Exception {
        User user =new User();
        user.setName("lihb");
        if("lihb".equals(user.getName())){
            throw new RuntimeException("开局就抛出");
        }
        try {
            throwTest2();
            user.setName("1");
        }catch (Exception e){
            user.setName("0");
            e.printStackTrace();
        }
        System.out.println(user.getName());
    }

    private void throwTest1(){
//        System.out.println("throwTest1");
        throw new RuntimeException("throwTest1");
    }
    private void throwTest2(){
        try {
            throwTest1();
        }catch (Exception e){
            throw new RuntimeException(e.getMessage());
        }finally {
            System.out.println("finally");
        }
    }

    @Test
    void jsonTest() throws Exception {
        String result = "{\n" +
                "    \"msg\":\"成功\",\n" +
                "    \"result\":[\n" +
                "        {\n" +
                "            \"HOUSE_ADDRESS\":\"舟山市定海区临城街道海天大道501号402室\",   //房屋坐落\n" +
                "            \"REALTYOWNER_NAME\":\"丁越佳\",                                 //产权人\n" +
                "            \"REALTYCERT_CODE\":\"舟房权证定字第1153888号\",                  //房产证号\n" +
                "            \"WFDJ\":\"C\"                                                    //危房等级\n" +
                "        }\n" +
                "    ],\n" +
                "    \"success\":\"true\"\n" +
                "}\n";
        ZsResponseVo<List<ZsWeiFangVo>> zsResponseVo = JSON.parseObject(result, ZsResponseVo.class);
        System.out.println(JSON.parseObject(result,ZsResponseVo.class));
    }

    @Test
    void sss() throws Exception {
        String url = "11111111";
        User user =new User();
        user.setName("lihb");
//        String dataJson = "{\"msg\":\"返回结果为空，请求url:" +"\",\"code\":\"99\"}";
        String dataJson = "<head><title>502 Bad Gateway</title></head>\n" +
                "<body>\n" +
                "<center><h1>502 Bad Gateway</h1></center>\n" +
                "<hr><center>nginx/1.16.1</center>\n" +
                "</body>\n" +
                "</html>";
        System.out.println(dataJson);
        System.out.println(this.dealData(dataJson,Map.class));
        System.out.println(this.dealData(dataJson, ResultResponse.class));
    }

    @Test
    void exceptionTest2() throws Exception {

        List<User> userList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            User user =new User();
            user.setName("lihb"+i);
            userList.add(user);
        }

            userList = userList.stream().map(item -> {
                try {
                    if ("lihb3".equals(item.getName())) {
                        throw new InternalServiceException("lihb3");
                    } else {
                        item.setName("lihb");
                    }
                    return item;
                }catch (Exception e ){
//                    return item;
                    return null;
                }
            }).collect(Collectors.toList());
        System.out.println(userList);
    }

    public <T> T dealData(String dataJson, Class<T> clazz) {
        Object codeObj = JSON.parseObject(dataJson).get("code");
        T t = null;
        String code = "0";
        String msg = "";
        if (codeObj != null) {
            code = String.valueOf(codeObj);
        }
        if (!"0".equals(code)) {
            msg = String.valueOf(JSON.parseObject(dataJson).get("msg"));
//            log.info("dealData调取服务失败或未查询到信息：" + msg);
            throw new InternalServiceException(msg);
        } else {
            t = JsonObjectUtil.converJsonToObject(dataJson, clazz);
        }
        return t;
    }


    @Test
    void sss1() throws Exception {
        User user = new User();
        user.setName("lihb");
        String sk = "2E4669D7ACC548A08E6E89ECFE67DAD02E0EED2419074557DF8B7C7FE3019482";
        String publicGk = "048EC4718A6D2DA9125582B751E6290874CBA5150B5AFDA957418071C282CC9BFE6DEA65ABC6ADB383CC39CCA3B7920010E2680BFB3DCF38022AC2471D3C62DEBA";
        //加密
        String jsonParam = "";
        try {
            jsonParam = URLEncoder.encode(JsonObjectUtil.converObjectTojson(user), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String encryptParam = "049965AD2C3C42E42DC9A799E7B8EB2075E94A4150FB6A8481867B178AD03F4AD3D27EB12346D9E4AEFB30F9A0B84521C72BB1D24F0AF535684AFC9A4AFAF4E73B363A1E2E092C45E9F6147798436F58469D7FA8BAA4AE333C096230A8278800098098C88F58E1D68BB6AFEB140BFD29FC73A86663290346E448DFE23D45B2E4174F393DFED88DCCBC501D80434FC63A930B170DD67C64740EF259E139997EF8994525C6AEEDE30AC7C1B0F16014B6E46CCB0A5D56443201DF3AF7145288EE52996ADEDEF4358B2C221732221CF2FECBFB5A71862503CCFE763B45EC2C2B64377111D849D4A84EE537B9442B4989139BAED6EAC2BEF9B37E36B958B88CA76C74BA293690CD9D87BBDC96D1240C9262520538B9831FDF1F349942F479BACCF20801BDE9682F2B1522A7D88777AC25A6D2F6C132F008F2E04969FD533C1B720DCEBF67893AFD56FC7CB4D6F5C7DC3C609B6A074F426F2BDD2753A93B5218899EE14137170185872A6F39E90939468DDAC7AF6C4A7B11733E54084F68205E446E7F22A31C16F2344E200EA4E82345ADCFE6BF44CA8216338D3F1AFE5C1A6377A935D0307B300B490E3AD01F04426E1B4FBAE76D55D224FFDDEBC3EACB14352739687C6FB7229ECCF5485994CE91DACE81A774706090784C6936FC42AC87D9B2BCB5E5F2A0D71C46867F3FF4F11C9A168EAA5AE39724BABF32895B55656D6D373B57EAC63FE3B4DD2FFCF77D1531CF7A126A3DC3DB39491C750AE4F34ADC628E52098FB2011A693A9A67E7EFE151E87CDFB2752473478DBB46C5EA35D12ABC10BB40A6631340E809A364633276C9D069753E8BCE012A26BE6E49311DA9C5FDB9F64415F2523354E50408CC9B4C2901731889DEB892F7463FA4214104C4D32A6C64E2B37AF752FC18C76EF153AC3C0BFF268B5A647C231311CBDC1B29D44BD7AFF16B0A7CF43B5D4E3F7C12F62240881DD8C580278E360F888CE1407B3FA6ED078EC800DE65E2D96A245918DC3B14D8D2F6C724382924964856ECCB1C9391C611B999DFC0F919FAA8BB3CD6DF7611A94FDF809BC1EBF3DC8257923A552A45AEF4B258FAF14B6D58D8F050BD8E6F428B0282BC28D8B00F85B9AC44E26B69527D935C59442A85983870298FF8694E4DFB4BD64DBF955BD3BF59E82F4FD6BC30ADA5FADF32F69E48295B76752C8AD40E2B93369FD3A2498AF8F7FB151B8A960164524771B770115A611C91FBA22588AEA227495132246F532A7B6B82483B70A27F2E15AF09F007F9DBE98EF9F684FFBD3EA3F126F92C572C7DCBA3E5093B2083FBEEACC1AE3461B912EDDE7B55948A872B04B73B1CB14031AAAA324D8562CEC8FD25542967E81E0A33395B5A138A4B3CBEB9432C01E6D1BC7790CBAA0697AB85CADF3A3B2EE8A2584A221E0B86E1D034C5521B4314A69FBFA9D2D000F1960897D118ADAEC0F6C2D7C6251744DB41568CBFF670727B94290FCD0C2BB55C8B6B8804264683795C5E7CE8DA84FB07ACF24A340E0303359F36766C9D1E4277F77B1CB853C901C222A9BBC1E14C307E5877E5508BB86F359D12E337E16F21814743F709A32B46DFA3C400C40A6AF23F330119DD241B02EE11DFB4AA6ADB88792FFF127159A1310BFEB636561AEEA7A861ED3D215AA94E2F4CA2778BE19950C185EBB18E96385CD85932EA0CA08EB815866D0396F02406C5D4B9F1C2247CAC273FB00299E9B80D4D93470F8D29D00B4FDF0919F3ECE47D8674DF84F78E85A47ED108D34E8D9E4C99660FBAF768370F9D56B30743FA091AFE79C6938EB971915972174A689850263C38FD64D907335CF0A267F53B0BA90842292CC18090C0927551B28612416229F56BCF6ED0384901C2D3E0CE2041EE295A89E57CFFF8FA728CB18A0D7CFEFB851B219FF94DBDACA1500FAB803523490F54F760D3436D8517DC12F1E1ACF7720E77665FB7076C782A55AB53444FFF01A84B84EAAE8CE963C3BE18FD8F61D241A7473F53DF1E80E188A60F3A1E9114FF5FC0DB2DC34396DACC31A1FCB9B8118A913AD1D02E0D69DEF9F5B0DA32F0842B43937462ECB7B5C99010BAB41E7C967E4498987E480DB94206EC4A59B1EC420965A513E561B4EEB332F70F1F1268A651C9E6C75984BEDB4A2969CEE5C5CBAF00DC41E4E7E9885433A9C27B66E0F5D8DA9C27055798534E688A8FBCEB1E09CB6EE208C6E089DACB3BF30472D919B2715B6672A938DA7CBBA09A1C06191D2B2A809CEFC5FE77D5E16131534966A2749CC6C2DE91C307BE9E2674AB945856CFE22DA3812EEEC82B2E2FA95AB2B2604F8E0C9654C4A29AB2B22C67196093634CF73334DD9E3367D2D72701566B4DBAEBA7C96D43A36D4D09BFCB3BB5F67D1085CFA491A0B8242A21C3D2979CD7018F83E8D4B30A0A134BA834220C010EF6FA53AA8D8FB478298258CD3824A7EA3D13DA1C5A34EB4BEDC624C688DDE0626900536DF422155547E4A7EFF7574E66E0252E918B58033BF94A2378F0B3665A0DE015E8FF0C0AF37D145E35A5F8E8BBA132F7346BD8CCD14DB7A42E984F6C75C9ABFD22C9DBCFF6F251A1B2F98FBEE4A8129193DD47109D1659A23E7665EA6BA00580A3852563B8FC2091C0C39ED68B984B625502D1CB5A07DFEE44DD78F7C6E6353ACC6473313D7F2807F572AFCEE66AB34878BB3FCFADEA5D64717984097DEE116ED9724B9664F720DCEE4A5193EE5BF49A9A1E9366B4D6061D6F9F9D181C63A12EFCCCF6D6674AF2C28910E7F185568FFB122FEC38ED9D27C8E2BA2E426F515781A0D2D750C474F2BC806B1C7041AA454FC5ABD98226E35DBE857CDDD4C2A377B9AE2AF6FBD26782511EC6EA58D610436BB2CE9608960F39FD96082C5E865B77AE7F2D02FBDA92C972F8E4386F2E2977D83201E17D33E1B8DAC5E60C20DA3930D62573F2000AF03E0E498955A81ED8F2764AF7461199314875B3EDC1AC1613D5AE22DEDC4F77A39725A665D4EB90FB604A01475370E3A85AB4C9F23E345E7074013011EE6837E9D0683F1A10DDE16226E33574DF7E28434AA9E24E09FA428BE05DEB5E63AA57F3C332B0DFBF030C670986179116B4C47B8DEE1B7D6C56AC0A6B266D6EFCB2733D89B3CEF8B8716136BD07BBA58BB9CDE9902C4BF9056CFDB7F043E2871E47FD4E97D64C4B59B2647B388C74F9DE3C4D2A2B6E92DCDB97C109AAA7B4B9567731F7114BE0ADB35BF0A3049090227D815BD734AF3690804D33C6B629B9B151126AA3791BAAA02958DA5829409D3D4BCEADA95E16E7C6DC1C20C2E5031C30FA67F4F062D55ECA6637DA6FC2BAA48344CC5BBBACD8E0EABB22B18BA0E5B56D5A4FCE353D21C2E9E4EB214D4DDB402CFEAB9C0EE6B07CD4BE6E2FCD2382E5F646A797BA33D128E09CE5A1BDC7F6E1CA6109BB5E84392E5B3835222DC17C9DEC9E47A4DE8D94B234DFA29FBA42756C518B4158205498DA1CB65C928F814C164D3582DFDD6015896C0152CB366C6C195AD3CCA1201DA4F9DC901255A8C038C72A85E340875F587D73DF4B610AD48A2D53D611F96A686E1F5A366F5BCE9BA4D8FB59DD40DB191B641AC1A6C719CA95570BA995FC533758488A5FF06DCD069405A4261B943BED448FFD2F51F3AB503692CD38F7263A972B19B1D8E8D7B55A9A78618132BB7C5396B562B122FED4530F8CBC939D613316FA89E5A91EF3D3132411A8A19BD8A28FD26845F643B8B1362EF3096BD79FFEBC42DABB39717756CFADB42C3DCA6A9FA64FE49D54D4663FF46513DA672B4439B62D746DAF92399DCAE91C214A1E5A580C1034D91D6A3B51F95A8D5198BAEEEC50DEDD65604A462E3EE6E54D6CE665919E97321AD2D8137DCD35355BCFB2DE9B586E8E39B73B5C530B582A3FAAA759F205AAA292E470B3950232F5389EDFFDAC6CB4F348AAFE622D7E0BCEEEE4C389FE1E88ECAEA7EC86EE3DDFD6166DCE97FB10F50647EA6BA7CF4368EA4830B540FC204CBD5E2649EEFCF74A975272BA548451F90DB3F4B0994C723E3F7A4C9A9718EF0FE9E286185563C463D2C32C1431842FD686E68D898E7FDB4741BEADB4E537850740A45FAC0381DAAE2C97B318AED56810203DEC1D4FFD183BB687302736F705F30673307A35D31DA3BC9AF45863B9D3623B7D370DEBA3ABDDCC05980E03FE9247A2A56685488C59E847D8004F2B5A28FE0BC97A273A61BA79C0029071B972B7F73090FC0A07C47DC059BDB716EB7A794E63D4B8AEC3A2EAA6F0BAD46D99062E156C9E955F160A4EAB13F2240283E12801DF039293BE0B704CA24AF5720A45E9770F485D618FA1FF244EB72464EFB8FE4AE0F671C5FB60CE3D3BD2FAE7264795694E703375162472743BA33FBE5F0863BF7EED67BDC718ED486A6FDC32DCA9308C02C48B3702FC2C621F95C590568FE8D4733B95665116C1D7C4070CC0010121B2DA1BDE5FED31387A931300E6E89A3883ACAE26A772002043A981865EB7775C52D2293C30560F5785CFCCEDECD9C5186C6E9A582F2081D2A878F80A3989EAFA2EF5D3BCDFF738F6AB395EBFC9AB76773F7AAC8CAC33903BADDE259825C675DB1408D0C5666033A3A407E1A6A6B6EE701106FCBD4ACA1FB26B0486CD0364219DF4FF4661D211A9D80BCEAF91004E8046EE338AEFD4AA0A945D014A1A04AF94F8CC62FD60709BD24297933A7A0BA8D81931DC965E01F0B9B8D5E397B3F1718CF9A3FF5D6DA1808A3969E1466A945434D06681C93B5A33D3740B7ACBD281F6AE2E22072D230D8B0C6A95084586292C0B6D0CFEEF7FF542517EDAD10B902F1B2E04B39BB2759B67A9E613CFCD190FA93ECB494F2FED2D1B4E5BFA1648B798C08B0615A1D20684447400C1E6032CBBE63B8DCDD6A452C134770935A21CDE7D5C4D77661465433CE0DD94C6B7EC1068DB7DB873FAC89931E06AFB5986A4AF12EE3D64EAE4BB04CFF2F300CD90EC8B4B64593BE2D0744588EB9358EB8461EF36D176F018EF313821877E1840230BDF8C639B00AA7859FDEAEE8F68ED161C63D0D45845932D081265BDEE751468F567FE85A72C9E607972398FB5C18D1EA868F28836495315D61647AC2F1DD639EA353AF3414B3C0152B698ABD38809196E1FADA68D61A2C89F7CEEAE75257265941EFEF838A57B07E2099881860652CE8BEDF0341D764B570C456D8ED89D4ECC07F9839F88FD392BF6465AA28A4076AF19E767496D5EBAF810AEC334C3C0EB8DF6494F815D6EE5B2F3C51E5DCCBF99EA4E1F582136FA514C7D626498B959B09D7B8C05CDB0A284AEEEBD2132395885F2FF0A9C2517C00638AFF9D50235C512B99159946EE84DEE4668F410986EE0D57A06EBECF2AB9A666C2A638B66C6A88024E836B2DED4B3AF87B1542846D9EBEFAC40A2644BD7DC7A79DF34A4CF908791D18841964AC3FD55773A14022FFC1385BD9E2FA8D2A050D5BD629261865BE5486BAA340669A526B27C4CA6B3A6FABE4A81310ECD39BFB99627F94278D3AA51F31224D25E0F3D5C9413DF457861E051F469E2489B777B2EF9B1D577B6113C909A99E82083F1264AC9959F58AE27C30199B2ED093F6D3C2F9176FDF6EE99805064D662E3E5B1B1F72098F3288986F71CE8C7EB113F8B585343EC848D37613048E163EDD1D47734E1DA7161C580CBAFA273801237A4D9041CF58ABEC641EBBDC5DD38BCA7DD8DB88A52C1BEDFE8D74CC5E24418982A03AF96C8BE1B08BD9A5F90BC8A392C4781752869100E7E563E5490ACCEC8EDE6E102EF6EDF4A218992A560B58721C56ADD51C01D21D1BF2CCFD5B44F38836ED8B1CEC8AAB9A6F44BC123CB9E835494AF81180C2E58FB3CB70D7A7881D972C7F251C5306AB43AF761C2CE0B8B5C510620A7C919A45C71A86F7D82C59F49231E18D34A0A62BC202937CBDA5728E6B45A20F51836B8F1736B3E7EE06DD5CB1F2AD9D6F7449125DA95FEE2EB8F649C60FEDDFA5D92DDAB320B95E4F092ACCB253EEBA3D69F6D926712593AEFBFC73E20409B2B4CB57D9B07C73964CF644BA9DC15D80F776DF3CC2E00C19686EDA54CBC05F79A78527A5B36B399D69C0482B3E327DDF429B73298E46A36CE6FEC8C69FB198E7A8231B020A69FA70446A97A8FB2A81078AB6ED213C881AFDA8CA4A1AC42FAFAB32129D0905985A59A9E0E5EADA9750B12B9248B93595B47C4D75670AB95ED9A0FBA3C774BFDBEED4805B714ACF9F1F8D856941774001D3BE2E0842ACF68BB4A7586B90566C8A352431BACA83801DFD5C02DC8AC6D7934C60A1C895E95ECD5EB85AF54558BB805377CFD57815D19E5C699284D2D4C21A8ADCB873CC76D3049E7362C4AF4DA5D177489848E8DE2E44B68C0A43B31CC66C4C6D56E1FAAA91FA6042B51D9BC5DA42100DA34EDE840BB2E0AE57FB4B78154E31515A19989E69710AE701EB1CEC426E1F1CF29D830420A760EE0929BEDC63A08E552F412261BA56210BFA4D310EC1BED591B67CCD800CF098D7891EB7E8D84696ECB742AE0B1C78C93127BBF7ED3B901CC7E60219A280BE6FA3B37124E53BE969FBD9F7AE4F15CF30B908CD48FC40EA07615C47834C7F055387FCDA2F0059CF868A0276AFD8EE941E48F865A44038A9CFB81026669A608B708721D239CD5E32DC58FDAD9F48EB737799FC4BC4CBBF0B07078B4873BE1BB25F0CF98C213BFEDD95EEE50400C46A8FDB80B2ACEA9FD7E22C4483117CFF6DA3DA243945ABA8182580980BEDBDD57896C8C0C36FB7F75D1EED4C23BF63D74C6842E58A8D9FED6126C368D27A34D0188E6DFE5F8C9D305AA880AAE4903375AE17857260739ED65EA408DADF60B9C8E3FE3120EE9B084218D975B0DE156E602803842461EB864C4D93F733D9A4B1E5B2058F0D2EAD8A7CD7D2372219188ABDC258232C86A044D3130BC8A3BEB6A5E094AB5FAAC999D140C4A91E31E38620B967463721CE7FDD671AC6EB14DC35262088F85DB9F8D8EDEE84F6A6056CEE4627599038AFC26F59ACF23D919D5D4C88772E8C475AEB4909B702AA24FCCFCE607284F64D0C97B026FA6333F4C001533DE5ABF3B2D89EA9AEDE3E230FD0919E5431E0170D32682C43D3D65FD27D52DD37F820983CF049CF5BCF543ADC68D3AEF7957AF7A87B0586626B68FF74BEB2B9834DEE7B87D688B027A0936C58B498ECA28A18C3B4E5A26B45E80B890B5F040F09B74B6D888E144BED3FFC6CDB400695D46ABE9D215F1876B062FFB4F2FD73E7562EAE8B5AB5B89921711FCD0DEF6A41725684554B18A090C9F35CDB8F6B8F031356E8B4076906E34BE601830A077FE7E6D69F3FC03B685FBB7907CD7DC4112942D6BC18E02EE405DE44979B8DF76834CF441414075C9C5C6D54A00CC78C8B304CBE3B0E0F159DB69B7E3CDFDE0F1068624C471C48B8FD6E97C127429BA4D3B3C9810B540AEFE74F54847153F91118F11CFC5C4097025EC5501560E8260CE6BB01A7AB16A7669E887A504FEAD4AA826A12220F80AAE70DC2192098B7292B07DA2BEEE7DF599B4E4121BDA11899E824C49B16EDF404F9A3F5F12863ABB32F589B0E483C251489633D0AB18F89C02BD0B4BA472C357F1A6142EA6269D4AF22770AE8E0F7CB0D42EA250BE2EC2076804CD1CD218DF5116F856B9C9AA88442FEA88C04B737309D6735C44FDA8849297D2DEC0C74D3394075480D3B080B10ECB924B38EE3045E2D243FE3BAAB91281FC129205915E27EDF8777931A683E48B0F062D1C63D7A019F3F0EB688FD567B15A6948EA7733A559C0B0840057F930205E5F4B7A0A753313C80E2F566D54CA89325F478CBA826291AEC1AC49BCAF7F4E34A1D7B0D281FDEAAE25EE25BC5E9757EC98799B4ABFFC50EE61A7A88FD8B59477756A840136BF2BD6DA7AF27ED95AC2359E0299CD58E8B45CE64D3E53D07F4FD7D407635FDA6847098855D31F128976703A1A9CDC570A89DAA68B489EA6614B0F0BEC9D68BD43C83AA19ADFB68094C8F92FEEEAF0FD867027B3A5B7A577772F38FE0484140AE2691220995240A301E9B0EAE98E7E64";
//        encryptParam = SM2Utils.encrypt(publicGk, jsonParam);
        // 解密
        try {
            String realJson = SM2Utils.decrypt(sk, encryptParam);
            realJson = URLDecoder.decode(realJson, "UTF-8");
            System.out.println(realJson);
//            rightInfoRequest = JsonObjectUtil.converJsonToObject(realJson, t);
        } catch (Exception e) {
            e.printStackTrace();
            throw new InternalServiceException(FailEnum.DECRYPT_FAIL);
        }
    }

    @Test
    void sss2() throws Exception {
        String temp = "[{\n" +
                "\t\t\t\"cfwh\": \"（2021）浙1081执3204号执行裁定书及协助执行通知书\",\n" +
                "\t\t\t\"cfjg\": \"温岭市人民法院\",\n" +
                "\t\t\t\"cfqx\": \"2021年05月20日至2024年05月19日止\",\n" +
                "\t\t\t\"djyy\": \"依温岭市人民法院（2021）浙1081执3204号执行裁定书及协助执行通知书查封。\",\n" +
                "\t\t\t\"cfyyf\": \"\",\n" +
                "\t\t\t\"cfsxh\": \"1\",\n" +
                "\t\t\t\"cfdjzxbsm\": \"\"\n" +
                "\t\t}]";
        List<Map<String, String>> qsCfxxResponses = JSON.parseObject(temp,List.class);
        Collections.sort(qsCfxxResponses, (map1, map2) -> {
            if (map1.get("cfsxh") == null || StringUtils.isNullOrEmpty(map1.get("cfsxh"))) {
                return 1;
            }
            if (map2.get("cfsxh") == null || StringUtils.isNullOrEmpty(map2.get("cfsxh"))) {
                return -1;
            }
            int diff = Integer.parseInt(map1.get("cfsxh")) - Integer.parseInt(map2.get("cfsxh"));
            if (diff > 0) {
                return 1;
            } else if (diff < 0) {
                return -1;
            }
            return 0; //相等为0

        });
    }


    @Test
    void sss211() throws Exception {
        String temp = "王怀双,顾露(341126198503185532,321282198907284062)";
        List<Map<String, String>> qsCfxxResponses = JSON.parseObject(temp,List.class);
        Collections.sort(qsCfxxResponses, (map1, map2) -> {
            if (map1.get("cfsxh") == null || StringUtils.isNullOrEmpty(map1.get("cfsxh"))) {
                return 1;
            }
            if (map2.get("cfsxh") == null || StringUtils.isNullOrEmpty(map2.get("cfsxh"))) {
                return -1;
            }
            int diff = Integer.parseInt(map1.get("cfsxh")) - Integer.parseInt(map2.get("cfsxh"));
            if (diff > 0) {
                return 1;
            } else if (diff < 0) {
                return -1;
            }
            return 0; //相等为0

        });
    }

    @Test
    void sss2111() throws Exception {
        String housesSaleResult = "{\"result\":[{\"HOUSE_ADDRESS\":\"?????????D3????604?\",\"REALTYCERT_CODE\":\"?(2017)????????0002071?\",\"HOUSESALE_ID\":\"75561f0ef93043c28205822cf92c3387\",\"HOUSESALE_STATUS\":\"已挂牌\"}],\"msg\":\"该房产不允许挂牌！\",\"success\":\"false\"}";        ;
        ZsResponseVo<List<Map<String,String>>> zsHouseSaleResponseVo = JSON.parseObject(housesSaleResult, ZsResponseVo.class);
        List<Map<String, String>> result = zsHouseSaleResponseVo.getResult();
        System.out.println(result.get(0).get("HOUSE_ADDRESS"));
    }


    @Test
    void ttt(){
        String content = "王怀双,顾露(341126198503185532,321282198907284062)";
        List<MsrVo> msrVoList = new ArrayList<>();
        this.sss(content,msrVoList);
        System.out.println(JSON.toJSONString(msrVoList));
    }

    void sss(String content,List<MsrVo> msrVoList){
        if(StringUtils.isNullOrEmpty(content)){
            return;
        }
        String[] strs = content.split("\\(");
        String names = strs[0];
        if(StringUtils.isNullOrEmpty(names)){
            return;
        }
        String zjhms = strs[1].replace(")","");
        if(StringUtils.isNullOrEmpty(zjhms)){
            return;
        }
        String[] nameArray = names.split(",");
        String[] zjhmArray = zjhms.split(",");
        String content1 = null;
        String[] telArray = new String[nameArray.length];
        if(!StringUtils.isNullOrEmpty(content1)){
            telArray = content1.split(",");
        }

        for (int i = 0; i < nameArray.length; i++) {
            MsrVo msrVo = new MsrVo();
            msrVo.setName(nameArray[i]);
            msrVo.setZjhm(zjhmArray[i]);
            msrVo.setLxdh(StringUtils.isNullOrEmpty(telArray[i])?"":telArray[i]);
            msrVoList.add(msrVo);
        }
    }

    void ss(String content){
        String zjhmRegex = "(\\d{15}$)|(\\d{18}$)|(\\d{17}(\\d|X|x))";
        String nameRegex = ".*(?=\\()";
        Matcher m = Pattern.compile(zjhmRegex).matcher(content);
        Matcher nameMatcher = Pattern.compile(nameRegex).matcher(content);
        System.out.println(nameMatcher.group(0));
        if (m.find( )) {
            int i = m.groupCount();
            for (int j = 0; j <= i; j++) {
                if(!StringUtils.isNullOrEmpty(m.group(j))){
                    System.out.println("姓名: " + m.group(j) );
                }
            }
        } else {
            System.out.println("NO MATCH");
        }

    }

    @Test
    void test1212(){
        String temp = "[{\"address\":\"定海区盐仓街道茗桂华庭9幢二单元403室\",\"tnarea\":78.67,\"xingzhi\":\"0\",\"fw_nm\":140050663,\"jzarea\":89.25,\"guihuayongtu\":\"住宅\",\"other\":null,\"zongcengshu\":5}]";
        ArrayList<Map<String,String>> arrayList = JSON.parseObject(temp, ArrayList.class);
        Map<String, String> stringStringMap = arrayList.get(0);
        System.out.println("address = " + stringStringMap.get("address"));
    }

    @Test
    void test12212(){
        String temp = "{\n" +
                "    \"result\": {\n" +
                "        \"images\": [\n" +
                "            \"/9j/4AAQSkZJRgABAaYrE0UUAOUnjNKAR3/WiigB8QznPapUjXOCB60UUDJEQYJHrU8Kbjz2oooAtLEgPIz+FXUQEA/jRRTEPCDinkDmiigEI3CDPJyB+fFODdgOlFFJgLIwAAA5z1rVsV+RSe+R+poooAvJ6Uo5x6f/WooqRjMYIxSE7X/CiigR//2Q==\"\n" +
                "    \"/9j/4AAQSkZJRgABAaYrE0UUAOUnjNKAR3/WiigB8QznPapUjXOCB60UUDJEQYJHrU8Kbjz2oooAtLEgPIz+FXUQEA/jRRTEPCDinkDmiigEI3CDPJyB+fFODdgOlFFJgLIwAAA5z1rVsV+RSe+R+poooAvJ6Uo5x6f/WooqRjMYIxSE7X/CiigR//2Q==\"\n" +
                "        ]\n" +
                "    },\n" +
                "    \"msg\": \"成功\",\n" +
                "    \"success\": \"true\"\n" +
                "}";
        ZsResponseVo<Map<String,String>> getImagesResponseVo = JSON.parseObject(temp, ZsResponseVo.class);
        Map<String, String> resultMap = getImagesResponseVo.getResult();
//        byte[] bytes = Base64.getDecoder().decode(resultMap.get("pdf"));
//        compactImage.setHtwb(bytes);
        if(resultMap.get("images") != null){
            List<String> imageBytes = new ArrayList<>();
            List<String> images = JSON.parseArray(JSON.toJSONString(resultMap.get("images")),String.class);
            for (int i = 0; i < images.size(); i++) {
//                 String image = images.get(i);
//                byte[] imageByte = Base64.getDecoder().decode(image);
                imageBytes.add(images.get(i));
            }
            System.out.println(JSON.toJSONString(imageBytes));
        }
    }

    @Test
    void test22222(){
        String ss  =  "{\n" +
                "    \"code\": \"0\",\n" +
                "    \"data\": [\n" +
                "        {\n" +
                "            \"cdbh\": \"1635753629923_1399BC7\",\n" +
                "            \"list\": []\n" +
                "        }\n" +
                "    ],\n" +
                "    \"msg\": \"操作成功！\"\n" +
                "}";
        com.gisquest.realestate.data.supervise.judicial.response.ResultResponse resultResponse = this.dealData(ss, com.gisquest.realestate.data.supervise.judicial.response.ResultResponse.class);
        System.out.println(JSON.toJSONString(resultResponse));
    }

    @Test
    void filed(){
        File file = new File("C:\\Users\\Administrator\\Desktop\\123.jpg");
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Test
    void throw21(){
        System.out.println(this.throw211());
    }

    public String throw211(){
        String str = "start";
        try {
            str = "throw1";
            System.out.println(str.toCharArray()[10]);
        }catch (Exception e){
            str = "throw2";
            throw new RuntimeException("throw4");
        }

        try{
//            str = "throw3";
        }catch (Exception e){
            str = "throw4";
        }
//        str = "end";
        return str;
    }

    @Test
    void throw221(){
        List<String> test = new ArrayList<>();
        for (String s : test) {
            System.out.println(s);
        }
    }


    @Test
    void throw22221(){
        if (JavaTest.ZT_NEW.equals(0)) {
            System.out.println("0");
        }
        System.out.println("1");
    }

    @Test
    void throw2222122(){
        Map<String, Object> logParam = new HashMap<>();
        logParam.put("test","test");
        Map<String, Object> logParam1 = new HashMap<>();
        logParam1.putAll(logParam);
        logParam.put("test1","test1");
        System.out.println(logParam);
        System.out.println(logParam1);
    }

    @Test
    void throw222(){
        String jsonParam = "JWeMZTINNJfBZ51hZxygMHpY1CHo5cl7HLXyGpLIc7gr1qZLDFUzN2dd+WCBKOESBjnjCmpX4MhSqWUJFR8JTWd8rwmQ7ZXmm1R75VZdPD/V9Y0D5oAvpqVFXHVUxGkOOjkaymmTE8+slkR9ewaEKkq3/D/V5hEE7f2k2hArz/UGs+OF/+ynhmYZ78my5e9LGF1RRKbGo9n8tTDy7p1U3KDyf7mGKnliFTwRc3ox5z61oTQtcmV0O+3iM3tqOt2F/ATrXa4KJc5klqy7hfoNziSRAy/VWkssD/kYL1ee7qw872SAi4KwLoqDH5k/f9zOxNKnFIyYKDL2+lpN7t4VLQCYnzTWAijh+/D3PhrgxGISkzuuEYnld7UoZSk1/cSlJsg7hQ2GKqgLSIUb9haX9LnzyAvr7siDz/BsVx4J/jrthlcPmTgHsfIERPRFvd1TeU5DPPyI/6OYz+cQ3IEx20ZqPIqyerpTw+X57Uo7CogJ7wv4Q6aUlF5P99qgwYx0";
        String jsonParam3 = "cSB+afxsAYS5HiZlkPFjS6D8rbpSrsNLrD3I0OJ7FKy6YanMJo4vvxWpOe4/kV1gzUKMwXB1SN6mpbePdIiSJkv5Zd8OBbVQ/FSbLBTvTTrkN1pd86uoOrCeyDX6NCudIC+k3jApg3OICyawpb1GjRNQyB9tyYK7T3uCITBgPOF242GNna/xhpAPnTnrYAJL4ZZhenx+0XGjbVTg0otQx8NUCYa0VQNfXWAlCIqlzX2MKiTAp6MfiNTr3Mj60+7hs8sP9f9LL2dQpRMIZPud2FEaZjTK3rqOpUTeY00FNbODFDQ9In6PWOC1CdxVxRzW3Flv1YfOgxltkqA2MhEftTJmG4+xA0WTAvap8uPo2DVDC4DftL7pEBvLlLuBkqgo+2pTFjPPqYlA96ndqBwOhls6+MNKijgv2mjQzHe17Ba/W/5a6OgM6MQxFtz77etwh7AoD9IolmzdvWl/Z256tMnOq5DTZWNo3JQ4IB0vxCQX8vWrFtWwNns95krNNEhI";
        String my = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCR8jjYg/vrGU0ozfFOpmIxG9rwyF+yrgs9TsJ9N8VXh4vUd4DYpQl77eXh3DjFA/DGWS8JDe/x6CqzLM+fJ8R2IbpTs2Cy/M9XG5iQWEUgDkcCrkF9WKMFCo7TL6IgN0O1QR0fh3sCZ36i1TJXfy3TdbF0bsiCpvolaEVthnvAQwIDAQAB";
        String my3 = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCOAvz2aIbOC1z+1tQqTatDf5zt34sXLWY187T8jB5sq519gWBF6pSwCX7vcMDZX4NLPY/T1C16wLfjcclA5UcZMniWKQ8o5DUmfcf4AHl4DIjTKQj3ejdRR/ebM2ppxfmEM8OYasANBcMT80TKiLJ+0LJbZy1EfXQYxbouzo9pxQIDAQAB";
       try{
           String encryptParam = RsaUtil.decryptByPublicKey(my, jsonParam);
           System.out.println("测试2:" + encryptParam);
       }catch(Exception e){
           e.printStackTrace();
       }
    }

    @Test
    void throw2222122111(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date parse = null;
        try {
            parse = simpleDateFormat.parse("2022-01-20 11:10:11");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(this.calculateDate(parse)){
            System.out.println("开始执行！！！");
        }

        List<Date> dates = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            try {
                dates.add(simpleDateFormat.parse("2022-01-20 0"+i+":10:11"));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        try {
            dates.add(simpleDateFormat.parse("2022-01-20 14:55:11"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Object[] objects = dates.stream().filter(this::calculateDate).map(item -> item.toString()).toArray();
        System.out.println(JSON.toJSONString(objects));
    }
    public Boolean calculateDate(Date date){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        int minutes = (int)((new Date().getTime() - date.getTime())/(1000 * 60));
        System.out.println("minutes:"+minutes);
        return minutes - 30 < 0;
    }
}

