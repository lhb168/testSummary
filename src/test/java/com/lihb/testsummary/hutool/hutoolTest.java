package com.lihb.testsummary.hutool;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ReUtil;
import cn.hutool.db.DaoTemplate;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.fasterxml.jackson.core.type.TypeReference;
import com.gisquest.platform.common.utils.StringUtils;
import com.lihb.testsummary.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SpringBootTest
class hutoolTest {

    @Test
    void contextLoads() {
    }

    /**
     * hutool转换测试
     *
     * @throws Exception 异常
     */
    @Test
    void hutoolTest() throws Exception {
        List<User> users = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            User temp = new User();
            temp.setLoginName("lihb"+i);
            temp.setName("lihb"+i);
            users.add(temp);
        }
        List<Map<String, Object>> userList = new ArrayList<>();
        users.stream().forEach(item->{
            Map<String, Object> stringObjectMap = Convert.toMap(String.class, Object.class, item);
            userList.add(stringObjectMap);
        });
        System.out.println(userList);

    }


    @Test
    void zzTest() throws Exception {
        String content = "aa{b}c{cdd}ef{g}h";
        List<String> resultFindAll = ReUtil.findAll("\\{.*?\\}", content, 0, new ArrayList<String>());
        System.out.println(JSON.toJSONString(resultFindAll));
    }

    @Test
    void strTest() throws Exception {
        String content = "[{\"ywh\":\"YJ-CCB-2021120100369\",\"lcdm\":\"5401\",\"thsj\":\"2021-12-02 09:20:43\",\"thr\":\"陈青\",\"thyy\":\"询问笔录使用新版本，有\"是否禁止或限制......\"\"}]";
        List<Map> maps = JSON.parseArray(content, Map.class);
        System.out.println(JSON.toJSON(maps));
    }

    @Test
    void str1Test() throws Exception {
        String content = "[{\"address\":\"普陀区勾山街道新津路7号和津苑2幢1302室\",\"tnarea\":64.54,\"xingzhi\":\"单位\",\"fw_nm\":239935,\"jzarea\":88.97,\"guihuayongtu\":\"商业用房\",\"other\":null,\"zongcengshu\":18}]";

        List<Map> maps = JSON.parseArray(content, Map.class);
        System.out.println(JSON.toJSON(maps));


        List<Map> houseList = JSON.parseArray(content, Map.class);
//        ArrayList<Map<String,String>> houseList = JSON.parseObject(content, ArrayList.class);
        Map<String, String> houseMap = houseList.get(0);
        if(!StringUtils.isNullOrEmpty(String.valueOf(houseMap.get("jzarea")))){
            System.out.println(Double.parseDouble(String.valueOf(houseMap.get("jzarea"))));
        }
        if(!StringUtils.isNullOrEmpty(String.valueOf(houseMap.get("guihuayongtu")))){
            System.out.println(String.valueOf(houseMap.get("guihuayongtu")));
        }
        if(!StringUtils.isNullOrEmpty(String.valueOf(houseMap.get("zongcengshu")))){
            System.out.println(String.valueOf(houseMap.get("zongcengshu")));
        }
    }

}
