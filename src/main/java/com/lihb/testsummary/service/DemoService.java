package com.lihb.testsummary.service;


import com.lihb.testsummary.entity.Demo;

import java.util.List;

/**
 * 测试(Demo)表服务接口
 *
 * @author 李海滨
 * @since 2021-04-30 17:26:40
 */
public interface DemoService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Demo queryById(Long id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<Demo> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param demo 实例对象
     * @return 实例对象
     */
    Demo insert(Demo demo);

    /**
     * 修改数据
     *
     * @param demo 实例对象
     * @return 实例对象
     */
    Demo update(Demo demo);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Long id);

}
