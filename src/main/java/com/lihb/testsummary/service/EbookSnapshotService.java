package com.lihb.testsummary.service;

import com.lihb.testsummary.entity.EbookSnapshot;

import java.util.List;

/**
 * 电子书快照表(EbookSnapshot)表服务接口
 *
 * @author 李海滨
 * @since 2021-04-30 17:27:08
 */
public interface EbookSnapshotService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    EbookSnapshot queryById(Long id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<EbookSnapshot> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param ebookSnapshot 实例对象
     * @return 实例对象
     */
    EbookSnapshot insert(EbookSnapshot ebookSnapshot);

    /**
     * 修改数据
     *
     * @param ebookSnapshot 实例对象
     * @return 实例对象
     */
    EbookSnapshot update(EbookSnapshot ebookSnapshot);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Long id);

}
