package com.lihb.testsummary.service.impl;

import com.lihb.testsummary.dao.DemoDao;
import com.lihb.testsummary.entity.Demo;
import com.lihb.testsummary.service.DemoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 测试(Demo)表服务实现类
 *
 * @author 李海滨
 * @since 2021-04-30 17:26:41
 */
@Service("demoService")
public class DemoServiceImpl implements DemoService {
    @Resource
    private DemoDao demoDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Demo queryById(Long id) {
        return this.demoDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<Demo> queryAllByLimit(int offset, int limit) {
        return this.demoDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param demo 实例对象
     * @return 实例对象
     */
    @Override
    public Demo insert(Demo demo) {
        this.demoDao.insert(demo);
        return demo;
    }

    /**
     * 修改数据
     *
     * @param demo 实例对象
     * @return 实例对象
     */
    @Override
    public Demo update(Demo demo) {
        this.demoDao.update(demo);
        return this.queryById(demo.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Long id) {
        return this.demoDao.deleteById(id) > 0;
    }
}
