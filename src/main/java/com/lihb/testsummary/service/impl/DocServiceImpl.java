package com.lihb.testsummary.service.impl;

import com.lihb.testsummary.dao.DocDao;
import com.lihb.testsummary.entity.Doc;
import com.lihb.testsummary.service.DocService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 文档(Doc)表服务实现类
 *
 * @author 李海滨
 * @since 2021-04-30 17:26:44
 */
@Service("docService")
public class DocServiceImpl implements DocService {
    @Resource
    private DocDao docDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Doc queryById(Long id) {
        return this.docDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<Doc> queryAllByLimit(int offset, int limit) {
        return this.docDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param doc 实例对象
     * @return 实例对象
     */
    @Override
    public Doc insert(Doc doc) {
        this.docDao.insert(doc);
        return doc;
    }

    /**
     * 修改数据
     *
     * @param doc 实例对象
     * @return 实例对象
     */
    @Override
    public Doc update(Doc doc) {
        this.docDao.update(doc);
        return this.queryById(doc.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Long id) {
        return this.docDao.deleteById(id) > 0;
    }
}
