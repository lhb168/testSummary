package com.lihb.testsummary.service.impl;

import com.lihb.testsummary.dao.EbookSnapshotDao;
import com.lihb.testsummary.entity.EbookSnapshot;
import com.lihb.testsummary.service.EbookSnapshotService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 电子书快照表(EbookSnapshot)表服务实现类
 *
 * @author 李海滨
 * @since 2021-04-30 17:27:08
 */
@Service("ebookSnapshotService")
public class EbookSnapshotServiceImpl implements EbookSnapshotService {
    @Resource
    private EbookSnapshotDao ebookSnapshotDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public EbookSnapshot queryById(Long id) {
        return this.ebookSnapshotDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<EbookSnapshot> queryAllByLimit(int offset, int limit) {
        return this.ebookSnapshotDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param ebookSnapshot 实例对象
     * @return 实例对象
     */
    @Override
    public EbookSnapshot insert(EbookSnapshot ebookSnapshot) {
        this.ebookSnapshotDao.insert(ebookSnapshot);
        return ebookSnapshot;
    }

    /**
     * 修改数据
     *
     * @param ebookSnapshot 实例对象
     * @return 实例对象
     */
    @Override
    public EbookSnapshot update(EbookSnapshot ebookSnapshot) {
        this.ebookSnapshotDao.update(ebookSnapshot);
        return this.queryById(ebookSnapshot.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Long id) {
        return this.ebookSnapshotDao.deleteById(id) > 0;
    }
}
