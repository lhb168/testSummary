package com.lihb.testsummary.service.impl;

import com.lihb.testsummary.dao.ContentDao;
import com.lihb.testsummary.entity.Content;
import com.lihb.testsummary.service.ContentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 文档内容(Content)表服务实现类
 *
 * @author 李海滨
 * @since 2021-04-30 17:26:37
 */
@Service("contentService")
public class ContentServiceImpl implements ContentService {
    /**
     * 内容Dao
     */
    @Resource
    private ContentDao contentDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Content queryById(Long id) {
        return this.contentDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<Content> queryAllByLimit(int offset, int limit) {
        return this.contentDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param content 实例对象
     * @return 实例对象
     */
    @Override
    public Content insert(Content content) {
        this.contentDao.insert(content);
        return content;
    }

    /**
     * 修改数据
     *
     * @param content 实例对象
     * @return 实例对象
     */
    @Override
    public Content update(Content content) {
        this.contentDao.update(content);
        return this.queryById(content.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Long id) {
        return this.contentDao.deleteById(id) > 0;
    }
}
