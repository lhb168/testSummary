package com.lihb.testsummary.controller;

import com.lihb.testsummary.entity.Doc;
import com.lihb.testsummary.service.DocService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 文档(Doc)表控制层
 *
 * @author 李海滨
 * @since 2021-04-30 17:26:45
 */
@RestController
@RequestMapping("doc")
public class DocController {
    /**
     * 服务对象
     */
    @Resource
    private DocService docService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public Doc selectOne(Long id) {
        return this.docService.queryById(id);
    }

}
