package com.lihb.testsummary.controller;

import com.lihb.testsummary.entity.User;
import com.lihb.testsummary.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Base64;

/**
 * 用户(User)表控制层
 *
 * @author 李海滨
 * @since 2021-04-30 17:27:13
 */
@RestController
@RequestMapping("user")
public class UserController {
    /**
     * 服务对象
     */
    @Resource
    private UserService userService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public User selectOne(Long id) {
        return this.userService.queryById(id);
    }
    @GetMapping("test")
    public String test(Long id) {
        byte[] bytes = new byte[] { 50, 0, -1, 28, -24 };
        String sendString = "";

        String encodeString = Base64.getEncoder().encodeToString(bytes);
        System.out.println("加密后："+encodeString);
        byte[] decode = Base64.getDecoder().decode(encodeString);
        System.out.println("解码后："+decode);
        return encodeString;
    }

}
