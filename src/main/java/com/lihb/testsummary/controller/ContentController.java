package com.lihb.testsummary.controller;

import com.lihb.testsummary.entity.Content;
import com.lihb.testsummary.service.ContentService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 文档内容(Content)表控制层
 *
 * @author 李海滨
 * @since 2021-04-30 17:26:38
 */
@RestController
@RequestMapping("content")
public class ContentController {
    /**
     * 服务对象
     */
    @Resource
    private ContentService contentService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public Content selectOne(Long id) {
        return this.contentService.queryById(id);
    }

}
