package com.lihb.testsummary.controller;

import com.lihb.testsummary.entity.Ebook;
import com.lihb.testsummary.service.EbookService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 电子书(Ebook)表控制层
 *
 * @author 李海滨
 * @since 2021-04-30 17:26:49
 */
@RestController
@RequestMapping("ebook")
public class EbookController {
    /**
     * 服务对象
     */
    @Resource
    private EbookService ebookService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public Ebook selectOne(Long id) {
        return this.ebookService.queryById(id);
    }

}
