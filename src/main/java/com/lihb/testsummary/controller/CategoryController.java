package com.lihb.testsummary.controller;

import com.lihb.testsummary.entity.Category;
import com.lihb.testsummary.service.CategoryService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 分类(Category)表控制层
 *
 * @author 李海滨
 * @since 2021-04-30 17:26:29
 */
@RestController
@RequestMapping("category")
public class CategoryController {
    /**
     * 服务对象
     */
    @Resource
    private CategoryService categoryService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public Category selectOne(Long id) {
        return this.categoryService.queryById(id);
    }

}
