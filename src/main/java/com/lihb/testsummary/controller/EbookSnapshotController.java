package com.lihb.testsummary.controller;

import com.lihb.testsummary.entity.EbookSnapshot;
import com.lihb.testsummary.service.EbookSnapshotService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 电子书快照表(EbookSnapshot)表控制层
 *
 * @author 李海滨
 * @since 2021-04-30 17:27:09
 */
@RestController
@RequestMapping("ebookSnapshot")
public class EbookSnapshotController {
    /**
     * 服务对象
     */
    @Resource
    private EbookSnapshotService ebookSnapshotService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public EbookSnapshot selectOne(Long id) {
        return this.ebookSnapshotService.queryById(id);
    }

}
