package com.lihb.testsummary.controller;

import com.lihb.testsummary.entity.Demo;
import com.lihb.testsummary.service.DemoService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 测试(Demo)表控制层
 *
 * @author 李海滨
 * @since 2021-04-30 17:26:41
 */
@RestController
@RequestMapping("demo")
public class DemoController {
    /**
     * 服务对象
     */
    @Resource
    private DemoService demoService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public Demo selectOne(Long id) {
        return this.demoService.queryById(id);
    }

}
