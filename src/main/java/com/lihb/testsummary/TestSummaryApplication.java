package com.lihb.testsummary;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.lihb.testsummary.dao")
public class TestSummaryApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestSummaryApplication.class, args);
    }

}
