package com.lihb.testsummary.utils;

import com.gisquest.platform.common.security.sm.Cipher;
import com.gisquest.platform.common.security.sm.SM2;
import com.gisquest.platform.common.security.sm.Util;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.math.ec.ECPoint;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class SM2Utils {
    private static final String KEY_PUBLICKEY = "PublicKey";
    private static final String KEY_PRIVATEKEY = "PrivateKey";
    private static final boolean withCompression = false;

    public SM2Utils() {
    }

    public static Map<String, String> generateKeyPair() {
        SM2 sm2 = SM2.Instance();
        AsymmetricCipherKeyPair key = sm2.ecc_key_pair_generator.generateKeyPair();
        ECPrivateKeyParameters ecpriv = (ECPrivateKeyParameters)key.getPrivate();
        ECPublicKeyParameters ecpub = (ECPublicKeyParameters)key.getPublic();
        BigInteger privateKey = ecpriv.getD();
        ECPoint publicKey = ecpub.getQ();
        Map<String, String> map = new HashMap(2);
        map.put("PublicKey", Util.byteToHex(publicKey.getEncoded(false)));
        map.put("PrivateKey", Util.byteToHex(privateKey.toByteArray()));
        return map;
    }

    public static String getPublicKey(Map<String, String> map) {
        return (String)map.get("PublicKey");
    }

    public static String getPrivateKey(Map<String, String> map) {
        return (String)map.get("PrivateKey");
    }

    public static String encrypt(String publicKey, String data) {
        try {
            byte[] bytesPublicKey = Util.hexToByte(publicKey);
            byte[] bytesData = data.getBytes(StandardCharsets.UTF_8);
            return encrypt(bytesPublicKey, bytesData);
        } catch (Exception var4) {
            throw new RuntimeException(var4);
        }
    }

    public static String encrypt(byte[] publicKey, byte[] data) {
        if (publicKey != null && publicKey.length != 0) {
            if (data != null && data.length != 0) {
                byte[] source = new byte[data.length];
                System.arraycopy(data, 0, source, 0, data.length);
                Cipher cipher = new Cipher();
                SM2 sm2 = SM2.Instance();
                ECPoint userKey = sm2.ecc_curve.decodePoint(publicKey);
                ECPoint c1 = cipher.Init_enc(sm2, userKey);
                cipher.Encrypt(source);
                byte[] c3 = new byte[32];
                cipher.Dofinal(c3);
                return Util.byteToHex(c1.getEncoded(false)) + Util.byteToHex(source) + Util.byteToHex(c3);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public static String decrypt(String privateKey, String encryptedData) {
        try {
            byte[] bytesPrivateKey = Util.hexToByte(privateKey);
            byte[] bytesEncryptedData = Util.hexToByte(encryptedData);
            return new String(decrypt(bytesPrivateKey, bytesEncryptedData), StandardCharsets.UTF_8);
        } catch (Exception var4) {
            throw new RuntimeException(var4);
        }
    }

    public static byte[] decrypt(byte[] privateKey, byte[] encryptedData) {
        if (privateKey != null && privateKey.length != 0) {
            if (encryptedData != null && encryptedData.length != 0) {
                String data = Util.byteToHex(encryptedData);
                byte[] c1Bytes = Util.hexToByte(data.substring(0, 130));
                int c2Len = encryptedData.length - 97;
                byte[] c2 = Util.hexToByte(data.substring(130, 130 + 2 * c2Len));
                byte[] c3 = Util.hexToByte(data.substring(130 + 2 * c2Len, 194 + 2 * c2Len));
                SM2 sm2 = SM2.Instance();
                BigInteger userD = new BigInteger(1, privateKey);
                ECPoint c1 = sm2.ecc_curve.decodePoint(c1Bytes);
                Cipher cipher = new Cipher();
                cipher.Init_dec(userD, c1);
                cipher.Decrypt(c2);
                cipher.Dofinal(c3);
                return c2;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}
