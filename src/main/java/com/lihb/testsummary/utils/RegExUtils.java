package com.lihb.testsummary.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Author lihb
 * @Date 2021/06/21-09:28
 * @Description 正则表达式工具类
 **/
public class RegExUtils {
    private static final String REGEX_LC = "\"access_token\":\".*\",\"refresh_token\"";
    private static final String REGEX_LC_REPLACE_START = "\"access_token\":\"";
    private static final String REGEX_LC_REPLACE_END = "\",\"refresh_token\"";
    private static Pattern pattern;
    private static Matcher matcher;

    public static String getLcToken(String input){
        String result = null;
        pattern = Pattern.compile(REGEX_LC);
        matcher = pattern.matcher(input);
        if(matcher.find()){
            result = matcher.group(0);
            result = result.replaceAll(REGEX_LC_REPLACE_START, "").replaceAll(REGEX_LC_REPLACE_END, "");
        }
        return result;

//        pattern = Pattern.compile(REGEX_LC_REPLACE_START);
//        matcher = pattern.matcher(input);
//        matcher.replaceAll("");
//        if(matcher.find()){
//            result = matcher.group();
//            System.out.println("out2:"+result);
//        }
//
//        pattern = Pattern.compile(REGEX_LC_REPLACE_END);
//        matcher = pattern.matcher(input);
//        matcher.replaceAll("");
//        if(matcher.find()){
//            result = matcher.group();
//            System.out.println("out3:"+result);
//        }

    }
}
