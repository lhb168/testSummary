package com.lihb.testsummary.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.util.JSONPObject;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

/**
 * 简单封装Jackson，实现JSON String<->Java Object的Mapper.
 * 封装不同的输出风格, 使用不同的builder函数创建实例.
 * @author ThinkGem
 * @version 2013-11-15
 */
public class JsonMapper extends ObjectMapper {

    private static final long serialVersionUID = 1L;

    private static Logger logger = LoggerFactory.getLogger(JsonMapper.class);

    private static JsonMapper mapper;

    public JsonMapper() {
        this(JsonInclude.Include.ALWAYS); //修改这里控制把对象转换成json数组，不管对象属性有没有值，都创建节点
    }

    public JsonMapper(JsonInclude.Include include) {
        // 设置输出时包含属性的风格
        if (include != null) {
            this.setSerializationInclusion(include);
        }
        // 允许单引号、允许不带引号的字段名称
        this.enableSimple();
        // 设置输入时忽略在JSON字符串中存在但Java对象实际没有的属性
        this.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        // 空值处理为空串
        this.getSerializerProvider().setNullValueSerializer(new JsonSerializer<Object>(){
            @Override
            public void serialize(Object value, JsonGenerator jgen,
                                  SerializerProvider provider) throws IOException,
                    JsonProcessingException {
                jgen.writeString("");
            }
        });
        // 进行HTML解码。
        this.registerModule(new SimpleModule().addSerializer(String.class, new JsonSerializer<String>(){
            @Override
            public void serialize(String value, JsonGenerator jgen,
                                  SerializerProvider provider) throws IOException,
                    JsonProcessingException {
                jgen.writeString(StringEscapeUtils.unescapeHtml4(value));
            }
        }));
//		//设置日期格式
//		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		this.setDateFormat(fmt);
        // 设置时区
        this.setTimeZone(TimeZone.getDefault());//getTimeZone("GMT+8:00")
    }

    /**
     * 创建只输出非Null且非Empty(如List.isEmpty)的属性到Json字符串的Mapper,建议在外部接口中使用.
     */
    public static JsonMapper getInstance() {
        if (mapper == null){
            mapper = new JsonMapper().enableSimple();
        }
        return mapper;
    }

    /**
     * 创建只输出初始值被改变的属性到Json字符串的Mapper, 最节约的存储方式，建议在内部接口中使用。
     */
    public static JsonMapper nonDefaultMapper() {
        if (mapper == null){
            mapper = new JsonMapper(JsonInclude.Include.NON_DEFAULT);
        }
        return mapper;
    }


    /**
     * https://www.cnblogs.com/zhjh256/p/6049663.html
     *  查找第一级的key属性,如果属性不存在,则返回null,属性值如果为明确的null，返回NullNode，否则返回正常的JsonNode

     注：JsonNode还提供了find/path/get等获取节点的方法，但是这三种方法都不能明确的区分节点不存在、为明确的null。
     所以，应该使用findValue方法。
     ((ObjectNode)targetNode).put("key","abcdefg1234567890");
     通过强制转换为ObjectNode，就可以对当前节点进行修改,其他的XXXNode均没有提供相关的API接口
     String modifiedJsonStr = mapper.writeValueAsString(rootNode);
     最后重新生成json字符串，这跟dom4j修改xml一样，只能重新生成，内置不支持直接修改原文件
     * @param json
     * @param key
     * @return
     */
    public static JsonNode getNode(String json, String key) {
        try {
            JsonNode rootNode = JsonMapper.getInstance().readTree(json);
            JsonNode targetNode = null;
            targetNode = rootNode.findValue(key);

            return targetNode;
        } catch (Exception ex) {
            return null;
        }
    }

    public static JsonNode getRootNode(String json) {
        try {
            JsonNode rootNode = JsonMapper.getInstance().readTree(json);
            return rootNode;
        } catch (Exception ex) {
            return null;
        }
    }

    public static Map<String, Object> json2Map(String json) {
        try {
            Map map =  JsonMapper.getInstance().readValue(json, Map.class);
            return map;
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Object可以是POJO，也可以是Collection或数组。
     * 如果对象为Null, 返回"null".
     * 如果集合为空集合, 返回"[]".
     */
    public String toJson(Object object) {
        try {
            return this.writeValueAsString(object);
        } catch (IOException e) {
            logger.warn("write to json string error:" + object, e);
            return null;
        }
    }

    /**
     * json对象转JAVA对象
     * 反序列化POJO或简单Collection如List<String>.
     *
     * 如果JSON字符串为Null或"null"字符串, 返回Null.
     * 如果JSON字符串为"[]", 返回空集合.
     *
     * 如需反序列化复杂Collection如List<MyBean>, 请使用fromJson(String,JavaType)
     * @see #fromJson(String, JavaType)
     */
    public <T> T fromJson(String jsonString, Class<T> clazz) {
        if (StringUtils.isEmpty(jsonString)) {
            return null;
        }
        try {
            return this.readValue(jsonString, clazz);
        } catch (IOException e) {
            logger.warn("parse json string error:" + jsonString, e);
            return null;
        }
    }

    /**
     * 反序列化复杂Collection如List<Bean>, 先使用函數createCollectionType构造类型,然后调用本函数.
     * @see #createCollectionType(Class, Class...)
     */
    @SuppressWarnings("unchecked")
    public <T> T fromJson(String jsonString, JavaType javaType) {
        if (StringUtils.isEmpty(jsonString)) {
            return null;
        }
        try {
            return (T) this.readValue(jsonString, javaType);
        } catch (IOException e) {
            logger.warn("parse json string error:" + jsonString, e);
            return null;
        }
    }

    /**
     *  //数组json转 List、复杂map
     List<UserBean> jsonToUserBeans = JacksonUtil.readValue(listToJson, new TypeReference<List<UserBean>>() {});
     Map<Integer, RbtCounter> srcMap = new LinkedHashMap();
     Map<Integer, RbtCounter> destMap;

     String jsonData = mapper.writeValueAsString(srcMap);

     正确：
     destMap = mapper.readValue(jsonData, new TypeReference<Map<Integer, RbtCounter>>(){});
     * @param jsonString
     * @param valueTypeRef
     * @param <T>
     * @return
     */
    public static <T> T fromJsonToObject(String jsonString, TypeReference<T> valueTypeRef){
        try {
            return JsonMapper.getInstance().readValue(jsonString, valueTypeRef);
        } catch (Exception e) {
            return null;
        }
    }

    public static <T> List<T> fromJsonToListObject(String jsonString, TypeReference<List<T>> valueTypeRef){
        try {
            return JsonMapper.getInstance().readValue(jsonString, valueTypeRef);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 構造泛型的Collection Type如:
     * ArrayList<MyBean>, 则调用constructCollectionType(ArrayList.class,MyBean.class)
     * HashMap<String,MyBean>, 则调用(HashMap.class,String.class, MyBean.class)
     */
    public JavaType createCollectionType(Class<?> collectionClass, Class<?>... elementClasses) {
        return this.getTypeFactory().constructParametricType(collectionClass, elementClasses);
    }

    /**
     * 當JSON裡只含有Bean的部分屬性時，更新一個已存在Bean，只覆蓋該部分的屬性.
     */
    @SuppressWarnings("unchecked")
    public <T> T update(String jsonString, T object) {
        try {
            return (T) this.readerForUpdating(object).readValue(jsonString);
        } catch (JsonProcessingException e) {
            logger.warn("update json string:" + jsonString + " to object:" + object + " error.", e);
        } catch (IOException e) {
            logger.warn("update json string:" + jsonString + " to object:" + object + " error.", e);
        }
        return null;
    }

    /**
     * 輸出JSONP格式數據.
     */
    public String toJsonP(String functionName, Object object) {
        return toJson(new JSONPObject(functionName, object));
    }

    /**
     * 設定是否使用Enum的toString函數來讀寫Enum,
     * 為False時時使用Enum的name()函數來讀寫Enum, 默認為False.
     * 注意本函數一定要在Mapper創建後, 所有的讀寫動作之前調用.
     */
    public JsonMapper enableEnumUseToString() {
        this.enable(SerializationFeature.WRITE_ENUMS_USING_TO_STRING);
        this.enable(DeserializationFeature.READ_ENUMS_USING_TO_STRING);
        return this;
    }

    /**
     * 支持使用Jaxb的Annotation，使得POJO上的annotation不用与Jackson耦合。
     * 默认会先查找jaxb的annotation，如果找不到再找jackson的。
     */
//    public JsonMapper enableJaxbAnnotation() {
//        JaxbAnnotationModule module = new JaxbAnnotationModule();
//        this.registerModule(module);
//        return this;
//    }

    /**
     * 允许单引号
     * 允许不带引号的字段名称
     */
    public JsonMapper enableSimple() {
        this.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
        this.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        return this;
    }

    /**
     * 取出Mapper做进一步的设置或使用其他序列化API.
     */
    public ObjectMapper getMapper() {
        return this;
    }

    /**
     * 对象转换为JSON字符串
     * @param object
     * @return
     */
    public static String toJsonString(Object object){
        return JsonMapper.getInstance().toJson(object);
    }

    /**
     * JSON字符串转换为对象
     * @param jsonString
     * @param clazz
     * @return
     */
    public static <T> T fromJsonString(String jsonString, Class<T> clazz){
        return JsonMapper.getInstance().fromJson(jsonString, clazz);
    }



    /**
     * 测试
     */
//    public static void main(String[] args) {
//        List<Map<String, Object>> list = Lists.newArrayList();
//        Map<String, Object> map = Maps.newHashMap();
//        map.put("id", 1);
//        map.put("pId", -1);
//        map.put("name", "根节点");
//        list.add(map);
//        map = Maps.newHashMap();
//        map.put("id", 2);
//        map.put("pId", 1);
//        map.put("name", "你好");
//        map.put("open", true);
//        list.add(map);
//        //String json = JsonMapper.getInstance().toJson(list);
//        //System.out.println(json);
//
//        Map<String, Object> map2 = Maps.newHashMap();
//        map2.put("id", 2);
//        map2.put("pId", 1);
//        map2.put("name", "你好");
//        map2.put("open", true);
//        String json2 = JsonMapper.getInstance().toJson(map2);
//        JavaType javaType = JsonMapper.getInstance().createCollectionType(
//                Map.class, String.class, Object.class);
//        Map<String, Object> map3 = JsonMapper.getInstance().fromJson(json2,
//                javaType);
//
//
//        System.out.println(map3.toString());
//    }

}

