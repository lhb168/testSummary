package com.lihb.testsummary.utils;
/**
 * Created by huangxp on 2017/8/15 0015.
 */


import javax.crypto.Cipher;
import java.io.ByteArrayOutputStream;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;


public class RsaUtils {
    /**
     * 默认私钥
     */
    public final static String DEAULT_PRI_KEY="MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAIN1OtuYxWv5yTZTnBiJLUjkN0cS2Qc+QPL1hHY9Z6oJnFfMX515sg10TuYWS3uKKiygPPzX5pnynAsZ0Cxn/4z/C1aeOE29LU6RM3DG6tpLCl4G7Z7+tgxTvFyw0rTw3hEGxNWNvQ4DrBD826eAPHcveY9sPVcw71qrm9NPqb1tAgMBAAECgYAiq1QufgfzBhlu3nYYcnUi+C0r7WiXZG+0o9Ofa2pfidFVxmyhF2tB6R61x8N4fWPKCSiMdlQ23akRtGkWjQb0Q3zo+efuLYGvAM3wYjXfBRbJGrfSgbSYkQBQ+EpJ0C04Mh8YVDIEI5n1A60DpqFmMw5xTwVboB81+RIhaBLT8QJBAMUDjjTuvh7WX69Bgp5NNGEhPxumZWcu1zg1WwcmvcDg76WL/9g1H5OcuS9eGHdTzERjcOChDHo8hy7wUvOe2CsCQQCq0Qh5JteFdWC3Nw1g+/LjmpaDd1+a2/RE7Uta8gWbWyXGEYTKSpt4UsFapH423is8xsi8ctcLYK5G0G8WIRzHAkEAln2hygM4TPv5+Th0WxGkWjF4MFnJiaj5E2GGWA8a0VPBG1a2puPla+9CcdFRpQzosrC3D49VpaXIqW1AIAINIwJBAIgpYni4hpoiy7qgTrDdhWr64jnfoT0fUiLTiPrw65lAWeb+NfzO5glO5kQr1VRFWu/ygTb+z/6IzcMzCSn5sScCQGC0v88cf3nJT82ASdT1D2/MpsRtsfenGQPAHOzJrQyPGi3CYzNLpFERuvi77UlAAkfRh75TxiuhVoDUaUUYACI=";
    /**
     * 默认公钥
     */
    public final static String DEAULT_PUB_KEY="MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCDdTrbmMVr+ck2U5wYiS1I5DdHEtkHPkDy9YR2PWeqCZxXzF+debINdE7mFkt7iiosoDz81+aZ8pwLGdAsZ/+M/wtWnjhNvS1OkTNwxuraSwpeBu2e/rYMU7xcsNK08N4RBsTVjb0OA6wQ/NungDx3L3mPbD1XMO9aq5vTT6m9bQIDAQAB";

    /**
     * 定义加密方式
     */
    private final static String KEY_RSA = "RSA";
    /**
     * 定义签名算法
     */
    private final static String KEY_RSA_SIGNATURE = "MD5withRSA";
    /**
     * 定义公钥算法
     */
    private final static String KEY_RSA_PUBLICKEY = "RSAPublicKey";
    /**
     * 定义私钥算法
     */
    private final static String KEY_RSA_PRIVATEKEY = "RSAPrivateKey";

    /**
     * RSA最大加密明文大小
     */
    private static final int MAX_ENCRYPT_BLOCK = 117;

    /**
     * RSA最大解密密文大小
     */
    private static final int MAX_DECRYPT_BLOCK = 128;


    private RsaUtils() {
    }

    /**
     * 创建密钥
     *
     * @return
     */
    public static Map<String, Object> generateKey() {
        Map<String, Object> map = null;
        try {
            KeyPairGenerator generator = KeyPairGenerator.getInstance(KEY_RSA);
            generator.initialize(1024);
            KeyPair keyPair = generator.generateKeyPair();
            // 公钥
            RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
            // 私钥
            RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();
            // 将密钥封装为map
            map = new HashMap<>(2);
            map.put(KEY_RSA_PUBLICKEY, publicKey);
            map.put(KEY_RSA_PRIVATEKEY, privateKey);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
        return map;
    }

    /**
     * 用私钥对信息生成数字签名
     *
     * @param data       加密数据
     * @param privateKey 私钥
     * @return
     */
    public static String sign(String privateKey, byte[] data) {
        String str = "";
        try {
            // 解密由base64编码的私钥
            byte[] bytes = decryptBase64(privateKey);
            // 构造PKCS8EncodedKeySpec对象
            PKCS8EncodedKeySpec pkcs = new PKCS8EncodedKeySpec(bytes);
            // 指定的加密算法
            KeyFactory factory = KeyFactory.getInstance(KEY_RSA);
            // 取私钥对象
            PrivateKey key = factory.generatePrivate(pkcs);
            // 用私钥对信息生成数字签名
            Signature signature = Signature.getInstance(KEY_RSA_SIGNATURE);
            signature.initSign(key);
            signature.update(data);
            str = encryptBase64(signature.sign());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return str;
    }


    /**
     * 用私钥对信息生成数字签名
     *
     * @param privateKey
     * @param dataStr
     * @return
     */
    public static String sign(String privateKey, String dataStr) {
        String str = "";
        try {
            byte[] data = dataStr.getBytes("UTF-8");
            return sign(privateKey, data);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }




    /**
     * 校验数字签名
     *
     * @param data      加密数据
     * @param publicKey 公钥
     * @param sign      数字签名
     * @return 校验成功返回true，失败返回false
     */
    public static boolean verify(String publicKey, byte[] data, String sign) {
        boolean flag = false;
        try {
            // 解密由base64编码的公钥
            byte[] bytes = decryptBase64(publicKey);
            // 构造X509EncodedKeySpec对象
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(bytes);
            // 指定的加密算法
            KeyFactory factory = KeyFactory.getInstance(KEY_RSA);
            // 取公钥对象
            PublicKey key = factory.generatePublic(keySpec);
            // 用公钥验证数字签名
            Signature signature = Signature.getInstance(KEY_RSA_SIGNATURE);
            signature.initVerify(key);
            signature.update(data);
            flag = signature.verify(decryptBase64(sign));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return flag;
    }


    public static boolean verify(String publicKey, String dataStr, String sign) {

        try {
            byte[] data = dataStr.getBytes("UTF-8");
            return verify(publicKey, data, sign);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    /**
     * 公钥加密
     *
     * @param key  公钥
     * @param data 待加密数据
     * @return
     */
    public static byte[] encryptByPublicKey(String key, byte[] data) {
        byte[] result = null;
        try {
            // 获取公钥字符串时,进行了encryptBase64操作,因此此处需对公钥钥解密
            byte[] bytes = decryptBase64(key);
            // 取得公钥
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(bytes);
            KeyFactory factory = KeyFactory.getInstance(KEY_RSA);
            PublicKey publicKey = factory.generatePublic(keySpec);
            // 对数据加密
            Cipher cipher = Cipher.getInstance(factory.getAlgorithm());
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);

            /*int inputLen = data.length;
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            int offSet = 0;
            byte[] cache;
            int i = 0;
            // 对数据分段加密
            while (inputLen - offSet > 0) {
                if (inputLen - offSet > MAX_ENCRYPT_BLOCK) {
                    cache = cipher.doFinal(data, offSet, MAX_ENCRYPT_BLOCK);
                } else {
                    cache = cipher.doFinal(data, offSet, inputLen - offSet);
                }
                out.write(cache, 0, cache.length);
                i++;
                offSet = i * MAX_ENCRYPT_BLOCK;
            }
            byte[] encryptedData = out.toByteArray();
            out.close();
            return encryptedData;*/
            return blockEncrypt(cipher, data);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 公钥加密
     *
     * @param key
     * @param dataStr
     * @return
     */
    public static String encryptByPublicKey(String key, String dataStr) {
        try {
            byte[] result = encryptByPublicKey(key, dataStr.getBytes("UTF-8"));
            /*
             * 加密后返回的是byte[ ]，打印出来会显示很多乱码，
             * 为了提高可读性，一般将byte[]转为base64编码，base64不是加密算法，是一种编码方式，
             * base64编码方式包括A-Z，a-z，0-9，+，/ 这些字符，编码时会把byte[ ]转成含有上述字符的字符串
             * */
            return encryptBase64(result);

        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }


    /**
     * 私钥解密
     *
     * @param data 加密数据
     * @param key  私钥
     * @return
     */
    public static byte[] decryptByPrivateKey(String key, byte[] data) {
        byte[] result = null;
        try {
            // 获取私钥字符串时,进行了encryptBase64操作,因此此处需对私钥解密
            byte[] bytes = decryptBase64(key);
            // 取得私钥
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(bytes);
            KeyFactory factory = KeyFactory.getInstance(KEY_RSA);
            PrivateKey privateKey = factory.generatePrivate(keySpec);
            // 对数据解密
            Cipher cipher = Cipher.getInstance(factory.getAlgorithm());
            cipher.init(Cipher.DECRYPT_MODE, privateKey);

            /*int inputLen = data.length;
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            int offSet = 0;
            byte[] cache;
            int i = 0;
            // 对数据分段解密
            while (inputLen - offSet > 0) {
                if (inputLen - offSet > MAX_DECRYPT_BLOCK) {
                    cache = cipher
                            .doFinal(data, offSet, MAX_DECRYPT_BLOCK);
                } else {
                    cache = cipher
                            .doFinal(data, offSet, inputLen - offSet);
                }
                out.write(cache, 0, cache.length);
                i++;
                offSet = i * MAX_DECRYPT_BLOCK;
            }
            byte[] decryptedData = out.toByteArray();
            out.close();
            return decryptedData;*/

            return blockDecrypt(cipher, data);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    public static String decryptByPrivateKey(String key, String dataStr) {
        try {
            byte[] result = decryptByPrivateKey(key, decryptBase64(dataStr));
            return new String(result,"utf-8");
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * 私钥加密
     *
     * @param data 待加密数据
     * @param key  私钥
     * @return
     */
    public static byte[] encryptByPrivateKey(String key, byte[] data) {
        byte[] result = null;
        try {
            byte[] bytes = decryptBase64(key);
            // 取得私钥
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(bytes);
            KeyFactory factory = KeyFactory.getInstance(KEY_RSA);
            PrivateKey privateKey = factory.generatePrivate(keySpec);
            // 对数据加密
            Cipher cipher = Cipher.getInstance(factory.getAlgorithm());
            cipher.init(Cipher.ENCRYPT_MODE, privateKey);


            /*int inputLen = data.length;
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            int offSet = 0;
            byte[] cache;
            int i = 0;
            // 对数据分段加密
            while (inputLen - offSet > 0) {
                if (inputLen - offSet > MAX_ENCRYPT_BLOCK) {
                    cache = cipher.doFinal(data, offSet, MAX_ENCRYPT_BLOCK);
                } else {
                    cache = cipher.doFinal(data, offSet, inputLen - offSet);
                }
                out.write(cache, 0, cache.length);
                i++;
                offSet = i * MAX_ENCRYPT_BLOCK;
            }
            byte[] encryptedData = out.toByteArray();
            out.close();
            return encryptedData;*/
            return blockEncrypt(cipher, data);


        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        //return result;
    }

    /**
     * 对数据分块加密，防止太长超出加密长度
     * @param cipher
     * @param data
     * @return
     */
    private static byte[] blockEncrypt(Cipher cipher, byte[] data) {
        int inputLen = data.length;
        int offSet = 0;
        byte[] cache;
        int i = 0;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            // 对数据分段加密
            while (inputLen - offSet > 0) {
                if (inputLen - offSet > MAX_ENCRYPT_BLOCK) {
                    cache = cipher.doFinal(data, offSet, MAX_ENCRYPT_BLOCK);
                } else {
                    cache = cipher.doFinal(data, offSet, inputLen - offSet);
                }
                out.write(cache, 0, cache.length);
                i++;
                offSet = i * MAX_ENCRYPT_BLOCK;
            }
            byte[] encryptedData = out.toByteArray();
            return encryptedData;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (Exception ex) {
                    throw new RuntimeException(ex);
                }
            }
        }

    }

    /**
     * 私钥加密
     *
     * @param key
     * @param dataStr
     * @return
     */
    public static String encryptByPrivateKey(String key, String dataStr) {
        try {
            byte[] result = encryptByPrivateKey(key, dataStr.getBytes("UTF-8"));
            return encryptBase64(result);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * 公钥钥解密
     *
     * @param key  公钥
     * @param data 加密数据
     * @return
     */
    public static byte[] decryptByPublicKey(String key, byte[] data) {
        byte[] result = null;
        try {
            // 对公钥解密
            byte[] bytes = decryptBase64(key);
            // 取得公钥
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(bytes);
            KeyFactory factory = KeyFactory.getInstance(KEY_RSA);
            PublicKey publicKey = factory.generatePublic(keySpec);
            // 对数据解密
            Cipher cipher = Cipher.getInstance(factory.getAlgorithm());
            cipher.init(Cipher.DECRYPT_MODE, publicKey);

//            int inputLen = data.length;
//            ByteArrayOutputStream out = new ByteArrayOutputStream();
//
//            int offSet = 0;
//            byte[] cache;
//            int i = 0;
//            // 对数据分段解密
//            while (inputLen - offSet > 0) {
//                if (inputLen - offSet > MAX_DECRYPT_BLOCK) {
//                    cache = cipher
//                            .doFinal(data, offSet, MAX_DECRYPT_BLOCK);
//                } else {
//                    cache = cipher
//                            .doFinal(data, offSet, inputLen - offSet);
//                }
//                out.write(cache, 0, cache.length);
//                i++;
//                offSet = i * MAX_DECRYPT_BLOCK;
//            }
//            byte[] decryptedData = out.toByteArray();
//            out.close();
//            return decryptedData;
            return blockDecrypt(cipher, data);
            //result = cipher.doFinal(data);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        // return result;
    }

    /**
     * 分块解密
     *
     * @param cipher
     * @param data
     * @return
     */
    private static byte[] blockDecrypt(Cipher cipher, byte[] data) {
        int inputLen = data.length;
        int offSet = 0;
        byte[] cache;
        int i = 0;

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            // 对数据分段解密
            while (inputLen - offSet > 0) {
                if (inputLen - offSet > MAX_DECRYPT_BLOCK) {
                    cache = cipher
                            .doFinal(data, offSet, MAX_DECRYPT_BLOCK);
                } else {
                    cache = cipher
                            .doFinal(data, offSet, inputLen - offSet);
                }
                out.write(cache, 0, cache.length);
                i++;
                offSet = i * MAX_DECRYPT_BLOCK;
            }
            byte[] decryptedData = out.toByteArray();
            return decryptedData;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (Exception ex) {
                    throw new RuntimeException(ex);
                }
            }
        }

    }


    public static String decryptByPublicKey(String key, String dataStr) {
        try {
            byte[] result = decryptByPublicKey(key, decryptBase64(dataStr));
            return new String(result);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * 获取公钥
     *
     * @param map
     * @return
     */
    public static String getPublicKey(Map<String, Object> map) {
        String str = "";
        try {
            Key key = (Key) map.get(KEY_RSA_PUBLICKEY);
            str = encryptBase64(key.getEncoded());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return str;
    }

    /**
     * 获取私钥
     *
     * @param map
     * @return
     */
    public static String getPrivateKey(Map<String, Object> map) {
        String str = "";
        try {
            Key key = (Key) map.get(KEY_RSA_PRIVATEKEY);
            str = encryptBase64(key.getEncoded());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return str;
    }

    /**
     * Base64解码数据
     *
     * @param key 需要解码的字符串
     * @return 字节数组
     * @throws Exception
     */
    public static byte[] decryptBase64(String key) {
        //return javax.xml.bind.DatatypeConverter.parseBase64Binary(key);
        return Base64.getDecoder().decode(key);

    }

    /**
     * Base64编码加密数据
     *
     * @param key 需要编码的字节数组
     * @return 字符串
     * @throws Exception
     */
    public static String encryptBase64(byte[] key) {
        //return javax.xml.bind.DatatypeConverter.printBase64Binary(key);
        return Base64.getEncoder().encodeToString(key);
    }


    public static void main(String[] args) {
        try {
        /*    String  encryptParam ="";
            String decryptParam ="";*/
            String publicKey ="MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDN5wN2/BTk0k63ClDmGctd27hIQhEHBPvUfSe/AITw7tQaigiTdim3fZHCPGxwc8XZlQ03QC0iyjP5MKcI+hAk/ZFHJv7nBarMZkeFj0Rk+OESot0GSH0DqrMwReexZPD1mAIp6snfWrryglND5lWmZCDxx0uFVNW4xtl0U6xXPQIDAQAB";
            //  String publicKey ="MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCQfFTPUkHvkllR/cKZMrbcAhWQoJTJt4Wv3p4KoGnBw2CiaWjFLkAJu8tX+UQoaHNYhQQUfK0iG4Ev0f/dcHadM7HdEfvndr9hHlgYyzuGEtVdIbzlpO2mf2sQN4hSxXMkD5/2dutVCe/lgu92ypV5j1rn0hwaUHV542AMfnAryQIDAQAB";

            String privateKey="MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAM3nA3b8FOTSTrcKUOYZy13buEhCEQcE+9R9J78AhPDu1BqKCJN2Kbd9kcI8bHBzxdmVDTdALSLKM/kwpwj6ECT9kUcm/ucFqsxmR4WPRGT44RKi3QZIfQOqszBF57Fk8PWYAinqyd9auvKCU0PmVaZkIPHHS4VU1bjG2XRTrFc9AgMBAAECgYBnqa3p4qIWTfeJWe0+26+ykmNmJbTy5sRux5ASLo2/y04ICoSk0THRiquuscMann72u38K2QmgYVLnW2JNN7CWNw50YbEKjIlc7mOpWAEDpgTOi7zNoeHQZC7bHeYHH7NEv9x5Yz4k0AUS/c6xm+iUzd0DbDeQV261lueXJGgGwQJBAPj+eBNiVhi0BsK6Td6l/4iWsdJxiArKmUfd40gKrs4jtYbk55he0zXls3xLaFWZ/hVtVVEjGEAzvO/mGXVE+zECQQDTsiaeEivhAiaGOgm8yBwoL1ehncxJ6duENZ2Vbe2Kq/oszbTnxxrGjqosPrdxloLCZsogtfzpkEM5IOIItAHNAkEAr7iOr0dn8XkjiEuI5weJhjCrgBPlaZA/iqt6Wf5IDWzAEFn9Y9gQAOqBt8dT842A9kg5vNAirjSVM0zVTKCJEQJBAKNTyI2eUXJJWXWn44leDrNGS2LqvwmrvuxS/WCjrmiV5O+aSd+k8tiMi2iVfqzJRds8uOfh4N5GqkayQxIZ52UCQCdgRDh8nK3mbQUTzD0xNEilC+70z2RD4R52ookiJp+bQh/IxfNzFSbsmiDeAcRSSeXjk9bU9EumEiUBqr9+wGM=";

            // String   encryptParam="WuiBVimlNW9qAz5dYTderzQbciOlVor9d1WHP0/qdu1URWPC3M0Ggn9i898g4qFzCcScFD7ttSdqy3B+nlJqklbfEjRR2kVHX5uhiMloDbp//Zx8Q1OpPAOOWes+cEW7gR0M2K1M3QQl3/INGBr7Lb5o0e1ISrYqFwm5mY4qnMZ/b/iDjeswZTEV8GmCBNeJTzD1GekD4NIqPjXen7DJqDeZs+aXwiyQkYJ6EJ2CEIp3RV92M3E//xgF1xB+Ase1BfjiV8YsLan6adFcZAexDj1DiEcDMoSCYcJQ3DzuErbeVa4W/AL0hjHzurXJj+6oC6/TeHnFggAgllbO4IUtn7FiVLpSRU3i1+3MdLE8ExrFZ7rSmWbUp6fv5rC3/cj4HnkoZklClptsM/yidTNrsktFAQyiQbk0Rdx3AKoshDzJ3D4JjcQVotwj6PsCJhaNd37eMtf8ymhLCqtS/1qnyI9NVLXmAsQwaBLEmB97hYYsrJl+GUyV3RGaOzHh98WgwLFhgQ/z00ZztiEMDVThzgfl/4TB3GSjB2930ak+wjVdhC3enrKndi4xUOPb/AyFhg/niuaKYMln1n1pAHFxkPzWpSCFeGVCrz+BU7EyW4id+QLw8MZDevVPWAsAk42bkBfWv4Of/hDdyQDhp4DdxgMkddodSTwsxAP5/mK1ESMlL8+GVydquI33w2ZLmy1vS4aKYnxjUmsTzX/P0EDq34t9iZ12bOZxj4dA8oaNydoof/AVhVefYFFMNEwWrQwYiNcvP179QswyqThXodXz1k/a0HLBPp3Lu+9EtkGTu80k+wo795BHW6c1FdRORFOHvY4xtrV25FGN7dJEFTWPkcG+oI5x/44k2QIjuAtgZi0SUKdUER9jos+wRstT3TPf/X5JLNfSy27oHBBTLw/UNloD74eCQNPgFv8d3jnImQv4rrO5XhLeic4yguswvcDv73/AZWqSqga2mcOYoiaLqBgWUCQeJjY3yMavpqvvuWx4CqD/G7WsJ8zDPVloe89fJkZApGfaGWjcoN19MSvn6HfEb41mcICfY4M0Bk0glo9UVuHs2memRZEL0YVJ8wY9nsRMuV4WxETFGPUFk+r91VULHwhXSnbbgViZZhSjYIj2XDnEwR4C3kMfudRGigYwjyw8A9cDCCSL+7VHPxN47MoxR+Pl2/xpK3zhBU0rJG0GmS4UJ5JR2/H6aYzluFvVf0Q1yXpYIFW3k3JZnFYeodrQKdHruSuPs4fIf8ago8OlkLuc+0uP86nBOOh0eYMFo2ImuXcdeWjg/4akTTdto/Y4vNGfoX5ZR169kll/TPA7Seb1O2zFee9UbMNB9uRdSNsxCPFQymukJzTSVD6evctCXpvPyNOBBPgiSw7d3tObKHOK8HCoZGND1jNYAiJjG9bzF4zKyT18y3nSmCH3yGwVy1p0h8FW+EV5i9dBlL/FAxcF3GltEv3i5uaCPaAqDyG417RaH+wv0CghL9S9PVQZ5MsQWwbqjJE326jtcCJJdJTeKxsiXqsxgUBPa/lxJzBTT78GrIoZvuF+eXmtnaWbHJTLn17UD5JcryloQDVMkg6tA1ER0CYQlEp8g7ptlyX6I9ZBYWEIL4vLsfMU/nyzpNxzgLFfd9Kk4agKr11497mFgVauGNyw0tkNm20WdN1pBvTTEutvCfIlKGEeDxgPCTqg7szqH3CXfMeZ6oDGDtFujt4UO/uAQvg9ESFH1fFBDwIQAdytn8gsxiu3AwP7Hrrvp7pMP2e4GWJZDbMGpmJMSVMbg7CyVmnYCJtvDbvguabWKG/vQNkNZ+nOYUAOlTADiFLYMo5Ea1PiR/AfbM4BlVnF6Z38c4jbAHmQ39jdWFD826ygyC6c40mRxJBdrDdVXwG95P/Y7/a2eGxpl3QWHG0f5U+3LjCsbJZYQaNQnyZTH4yCjEWgaEKWm1LVMgPGlDlJfM9f33L1pRFY2cGMUza7mqQkBg1Jr2iojW5015PLYgLjsPzb9D10npIc8KC1QSm4gEcN4WhqcQpRch14MPSIWPkqGBH3mBJ6vW/mG8lplPBFms22zMCsWdeh06+0p7NEedArasWAYMr/AmapshIg57l3clpQw1dVnEG4bkfB7X1Re8CS2F6NPuYnGzGtCcp39tCd1nzsiCWHgyuDX84a9aoEA809DkBJP6oBhdl8IR/bF0Kyg1F78UvDAutVVnilvESyvJ2cogBTSgIv7nAgWkKwiYR7gtnuho93SAd39JLXcoPF7Xva0l+h2zh234wcJW+Viil/aUTGOpfYbM9lQaA/Ggze7qCswsQlysmJezvI5pPf7lISI6UqZbzwaOLCvBxpgKmmQKTTjYvGQ9PVMaf6FDpm6oMWJ6spByZSrcCHT9/9S2L9XHAPCA9DAzilikKNda7gOYF3lUxQM13Ih6m4UjSyi0Sj41CVbAW4ADkFNSqMtCMhyXYG4T/2lwNgbAB5zpj4HbyS0jGvY9y6frfcoh0EZeIGUrGqCXJ7cHgTa15qNZQTKMO2ZLC/wZQpIos3DJ8ipEsKfK2yFP19qbV4aGHdn/9vzNohmFDWZ2PA2wMhb9rVOKloPFTik62NVBhSs+7qzsyZ4Mh3RKHBqFLwoUzdQ6LdtD3jaxYBO112hpKZSZjjV3ngrrsTjP2bZINL1b1RoNOYaMP9gHLx+Fzvn7S+QPaobRYvT82OEG/k3AMK/PdY9jUSANnVG9WON1IwN5GuUlMPuoxYZ3waPcQbTs4ZxW2mR1GeGMPWdmAbGEC7tcYQ3l1vpqaJjYrN2ipUBFH/1pYEShxPrfJHRyCC+8QNGsrOojMf27/f0R6foXnWlw0ezhmqKXn3v1kbzLCWVpKODLX3Bbn1nxRW0AzlEAGFKptH47uqaHsD8lXvx4vpHb/Ya4INhJlQoNB4GkVGTqbSX6S5w39vMOhDqevZDGJPzo2xaj0Oy8w6W9CIQ6tVrTzs06brhDbdw2Eoxbsrofs1UeaxvOnsyulx+sjAR/V6BsO5/930aAjESgtNBfw4K1KESLTBebJaCBsZuM9rhvVw0KKRyWsyy9EX4h656scuAbmSF05pySvNwujc9ivRn2hohY/omDsFZHH98Y1qAgEsNGVGp56mWfDKNVNC7/EOalqOLSpc6hiSn7F4Db/ULJ7S8nL4hMlCzCNVQ8H84fRIUqmGkXuJlcDPeX+GoslIlSPLdX3BYWuFFOuoT9IfMx/qNNL64l2rU9mbXp+0F/862XNuI4vPmQg00DHcNd2PVyxN2uKbrAYucLxy1HRh/HIDysuiPZidrXBzr1+RQEH+x58hnKJAOAc/t9NJw0g2XzDbG2OViDUjs/xTxN2whfQldQAjcK6wxcLBBzP557PWNxMRAGqy8CXQ5un3OP6xKpY1gCW4sGP97f46AJvlJ0Ef2A==";
            //  String   encryptParam="iVW0OvuQiE4+lwKsv+SeMwu50jDNV5U81kNCxb52U3JpZ2JjnSicADbhuV/EMR1s2TMWAmoEkrLQHukUMNOqh2RKeS9/1Zt3khgX7gOtZfoxRnMVgrI8hUBST9dEdbNS/3rK0nRK0XhZjb7ajjSePYE3hiwbTNfL6UcPNnzcgDkHY17I8i/EKd0XDnarn8y5Hsn6ZuZSsc47m3T4FvP/q7lXzhX6ABN4i2BxA41t/e4H4HDUVf4mqejaXfsFM+o+HDiEH4NzEVs4UeqTqzMdYzWm507LwOOZKxK8IaDgCwTL3aWsPUqzoKalTYR4heAlon8nuWh1XNZP9AxuR85KCFEV9aFbCFS0hhZivh22UrfCLujG/WR748TBtXNuyRsiigCVmuJC6mzbAmYE9+ka6eoQnUP5mMbYpJwoJFJO6SxnQAyAjMAXdOoX0MMoUuGWoSurCUEX8cO0syefcKfb+tMj5pDyYtkfX9y03DtUk/HCK48DndUMYdOVBdjG5LSLMCl28Ar4HZTe8Oc0kdW+luWnPNR/KCXcPvd7Uy33iyE2Yex9sLzQukFcDVxsShazCyzei+8axCN5rYb1Up8l6yvPSpvMp0lcJL+te3NvukiVAWeJHGEzwoI5QD2HORH1rOLp94aF/EWxSsMVzjHOYJ9TZeo1Sli78M9wVrSJu5GIuXtUCCYkr33l/oZuFS1nDV22S7ilnjpY29uSB7yv89Gr9wsRYdtnEOFwHBB4ZkEJnCrlPEHN98yHSgj8l0hGks9R51Y1wBPbkkqWidx5VoVSdS9mVCIVC2E2HdpFYpnmzeL2DTbeFsv3C8wYfBPv4CwRKEKKQ/5TZCbZx5DaJjBp6H9M/boZFKxGfWMg/4PTrhPwqqJ/Gs+twdE9N7EJe/6fnJ9iNWMYCptD9chgJL/e0Ra2/45PdJRaUWf5INb6AgdTWnLZZt+7YwljWfLuvt1ntvXjLoM4thLWWcc7JRJWvFTbWglm9kCKHoFcpC2zmQRUQ4BU+nKUfi6COaN4bVOQm+WBELp1h8n4gr1Yy321TZGwChT0lMV+WeJDsH4kB+kRSon4OUnEOmvG2bLl6ffnTg4sthlmTYkIo14MzrJh2fC2RnFvrtyRbPoqvOsyfHEwTRYyfHkr/biN7lodQdmQNO2u7lIST+h5NsjnWhBsPS5NxhQZUQqI1i1FJzhW26EhFRvt6chj9AWZDPhhia6VCYvSlnGJbhv0HzngQCJztSuA1Nwlmyl3B6AjoK0X6s1U87s0esKvOQWB0ezdRJjdWwZwowUNykCyo7Z38CZ4z0IfCnXl/9FKHPuUFckS0dEJJocCRxK+zt8KzkeMO0DA1kQRnuLbRB2cOKNJLzcGQNHExgDTypbO0hEvvCMEXtJNfALy2P04dL2TWNYNW8WRjhjEKnCAkOIcwh5Lo1T/sJINZ24vRcuSzPenx4h01Q9mmZgyoRqZdQVoDr0VreZ60P9QZqtxrFnlG0kMgsW/y0swaoxKR9sM502GGM40391tqDzAmub7BpSzSE/vIK2eXxhgjlSsknbTKdhD+lKArxSB+QxvTUUafsOp8FCG1/OXMAuvSyszCwH56aLpc/0kk4BCws7B5Q05z68EdUPi+gItj30niJLSf76xQSI9tVRIt8Zb1eWbhGEKNd+lnFCy9DRW8o2T4E2fgvhrvHrBBwW/XS4p8OYP1Ltpe94bKl6Xn32BABLIBlnJ1g9nzAjD5a0Yo5olMIJJmiCRx/v7n0wa/a0BDnryozvKk6xXx1zeu6VzHrkszujc6ifa/kkG95YRPoB2E5kBhfDvOwBAfR5lU5ylGOE0PYnlzt/lIeF3e1aW0cciSSUjWu2M3rGLANQoPYl0tqyM88ZdC2kb/r1NftCiQiumKu7uPvsmi3C2A1UbpE8cEx7hYf6ElZL0pl1cpPEiBZln8gLtlZmAT6AeciSZISNTEUWdJiHyxrs3FL1x9xsVrqwon+DYjWqxy83wjFkm693jvQwbnN4ActEBLIn1/6kEDsE1EhVQh4gfNl3ocRYKu18+Oz/Bat8fJpZlC0zl1KGEc2m9ueH7rTDfDSu27sa9YWW/MftOaG2tszh/HFqJhQCCmUH+nJcPTtQCCNon4sV7gQQLbfOW43OOO6oA80FW8mZ1xzD/2D5+6MW3P/zBBoEkrlDZ4t1wvvnOctFHZ90I1HcXj4plj1Z11qNe9y8loTgU2LVzMQrT7jdSdQbRMxR9I9cppPplyF3qPxj3FGrqraKb5ruaCF+AVUqQ6LCYpPJoXJI+S/ScYvJkxyWNtClAQ++sqemnkZXv9V8iS8xGO9rDGDzKL4p0MvBRUXoWjydN3XHbhIxLOgJRM2cQO9upzs2FeJ+YEyxdu1bCaddgHaP1t3D+BMwsUkmoMLuOMNR7QnDhwzNAa/EAsa7LEYFg5fDEXP6qFRVBkwTGUEsFQx6tMjdzyi4wCTDHMVQcAzthT6Alk8tFTJW94WtWX/4gzVt+9qfcCzkUlL1NKblev8+nzjSWOD/WvohPoAHD/n/ivF1GFoDDE5lU82YJ+B2bqR3+";
            String   encryptParam="C2opPvZtIhqF+nxIU2YmwDcU9DjPCzitboP6zID2bun9vrKsiraUujjW78EwJ/ErM8oZ74b+biyymsxUwF3VBaVJqVuEJ//GXXry0rfLtLOHWZH/m37hW4A7RVAuDrFI7R6x8hBBUdLUcZO/OKE71sen/u+/XmlU8nXUBfNK07U=";
//            decryptParam = decryptByPrivateKey(DEAULT_PRI_KEY, encryptParam);
//
//            Map<String, Object> map = generateKey();
//            String privateKey = getPrivateKey(map);
//            String publicKey = getPublicKey(map);
//            System.out.println("privateKey:"+privateKey);
//            System.out.println("publicKey:"+publicKey);
//
//
//            Map<String, String> mapParam = new HashMap<>();
//            mapParam.put("user", "省国土资源厅");
//            mapParam.put("password", "phz");
//            mapParam.put("req_ip", "127.0.0.1");
//
//
//            String jsonParam = JsonMapper.toJsonString(mapParam);
//            //加密
//            // String e = encryptBase64(jsonParam.getBytes("UTF-8"));
////            byte[] d = decryptBase64(e);
////            String str = new String(d);
//

            String  dddd="1234";
            encryptParam = URLEncoder.encode(dddd, "UTF-8");//进行编码，防止中文乱码
            encryptParam = encryptByPrivateKey(privateKey, encryptParam);
            System.out.print("加密内容："+encryptParam);
            //加签
            //String strSign = sign(privateKey, encryptParam);


            //验签
            //boolean boolSign = verify(publicKey, encryptParam, strSign);
            //解密h
            String  decryptParam = decryptByPublicKey(publicKey, encryptParam);
            decryptParam = URLDecoder.decode(decryptParam, "UTF-8");//进行解码，防止中文乱码





            System.out.print("解密内容："+decryptParam);
        } catch (Exception ex) {
            System.out.print("");
        }

    }
}
