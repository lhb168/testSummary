package com.lihb.testsummary.entity;

import lombok.Data;

/**
 * 舟山查询挂牌、合同信息、合同提交接口返回VO
 *
 * @Author lihb
 * @Date 2021/8/17-11:45
 * @Description
 **/
@Data
public class ZsEsfInfoCommonVo {
    /**
     * 房产证号
     */
    private String REALTYCERT_CODE;
    /**
     * 地址
     */
    private String HOUSE_ADDRESS;
    /**
     * 状态
     */
    private String HOUSESALE_STATUS;
    /**
     * 挂牌内码
     */
    private String HOUSESALE_ID;
//    /**
//     * 合同内码
//     */
//    private String SALECOMPACT_ID;
}
