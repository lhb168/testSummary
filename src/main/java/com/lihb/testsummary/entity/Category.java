package com.lihb.testsummary.entity;

import java.io.Serializable;

/**
 * 分类(Category)实体类
 *
 * @author 李海滨
 * @since 2021-04-30 17:26:22
 */
public class Category implements Serializable {
    private static final long serialVersionUID = 758117798869434855L;
    /**
     * id
     */
    private Long id;
    /**
     * 父id
     */
    private Long parent;
    /**
     * 名称
     */
    private String name;
    /**
     * 顺序
     */
    private Integer sort;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getParent() {
        return parent;
    }

    public void setParent(Long parent) {
        this.parent = parent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

}
