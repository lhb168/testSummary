package com.lihb.testsummary.entity;

import java.io.Serializable;

/**
 * 测试(Demo)实体类
 *
 * @author 李海滨
 * @since 2021-04-30 17:26:39
 */
public class Demo implements Serializable {
    private static final long serialVersionUID = 892579762274972398L;
    /**
     * id
     */
    private Long id;
    /**
     * 名称
     */
    private String name;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
