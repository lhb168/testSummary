package com.lihb.testsummary.entity;

import lombok.Data;

import java.util.List;

/**
 * @Author lihb
 * @Date 2021/06/18-12:10
 * @Description
 **/
@Data
public class LcResponseDataVo {
    /**
     * 合同信息
     */
    private List<LcContractVo> contract;
    /**
     * 出让人信息
     */
    private List<LcJyrVo> ceder;
    /**
     * 受让方信息
     */
    private List<LcJyrVo> owner;
    /**
     * 房屋信息
     */
    private List<LcHouseVo> house;
}
