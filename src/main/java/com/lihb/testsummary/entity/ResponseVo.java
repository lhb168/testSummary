package com.lihb.testsummary.entity;

import lombok.Data;

/**
 * @Author liuxn
 * @Date 2021/1/19-17:24
 * @Description
 **/
@Data
public class ResponseVo<T> {
    private String code;
    private String msg;
    private String message;
    private T datarsa;
    private T data;
}
