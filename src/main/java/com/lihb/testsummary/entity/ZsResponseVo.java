package com.lihb.testsummary.entity;

import lombok.Data;

/**
 * @Author lihb
 * @Date 2021/8/17-17:24
 * @Description
 **/
@Data
public class ZsResponseVo<T> {
    private String success;
    private String msg;
    private T result;
}
