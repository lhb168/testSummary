package com.lihb.testsummary.entity;

import lombok.Data;

/**
 * @Author lihb
 * @Date 2021/06/18-12:10
 * @Description 聊城返回结果->交易人信息
 **/
@Data
public class LcJyrVo {
    /**
     * 交易者全称
     */
    private String jyzqc;
    /**
     * 交易者证件名称编码
     */
    private String jyzzjmcbm;
    /**
     * 交易者证件号码
     */
    private String jyzzjhm;
    /**
     * 法人代表/交易者类别
     */
    private String jyzlbbm;
    /**
     * 所占份额
     */
    private String szfe;
}
