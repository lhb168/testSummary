package com.lihb.testsummary.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName ResultResponse
 * @Description 返回结果
 * @Author chenwl
 * @DATE 2020/10/27 10:19
 * @Version 1.0
 **/
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ResultResponse<T> {
    private String code;

    private String msg;

    private T data;

    public static ResultResponse success(Object data, String msg) {
        ResultResponse resultResponse = ResultResponse.builder()
                .code("0").msg(msg).data(data).build();
        return resultResponse;
    }

    public static ResultResponse success(Object data) {
        ResultResponse resultResponse = ResultResponse.builder()
                .code("0").msg("success").data(data).build();
        return resultResponse;
    }

    public static ResultResponse success() {
        ResultResponse resultResponse = ResultResponse.builder()
                .code("0").msg("success").build();
        return resultResponse;
    }

    public static ResultResponse fail(String code, String msg) {
        ResultResponse resultResponse = ResultResponse.builder()
                .code(code).msg(msg).build();
        return resultResponse;
    }

    public static ResultResponse fail(String code, String msg, Object data) {
        ResultResponse resultResponse = ResultResponse.builder()
                .code(code).msg(msg).data(data).build();
        return resultResponse;
    }
}
