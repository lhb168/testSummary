package com.lihb.testsummary.entity;

import java.io.Serializable;

/**
 * 用户(User)实体类
 *
 * @author 李海滨
 * @since 2021-04-30 17:27:12
 */
public class User implements Serializable {
    private static final long serialVersionUID = -64000697551881501L;
    /**
     * ID
     */
    private Long id;
    /**
     * 登陆名
     */
    private String loginName;
    /**
     * 昵称
     */
    private String name;
    /**
     * 密码
     */
    private String password;
    /**
     * 排序
     */
    private Integer sorted;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getSorted() {
        return sorted;
    }

    public void setSorted(Integer sorted) {
        this.sorted = sorted;
    }
}
