package com.lihb.testsummary.entity;

import lombok.Data;

/**
 * @author :liuxn
 * @date : 2019/10/15
 */
@Data
public class MsrVo {
    /**
     * 标识码
     */
    private String id;
    /**
     * 合同Code
     */
    private String htcode;
    /**
     *姓名
     */
    private String name;
    /**
     * 证件号码
     */
    private String zjhm;
    /**
     * 证件类型
     */
    private String zjlx;
    /**
     * 联系电话
     */
    private String lxdh;
    /**
     * 购房人类型
     */
    private String gfrlx;
    /**
     * 联系地址
     */
    private String lxdz;
    /**
     * 共有方式
     */
    private String gyfs;
    /**
     * 共有份额
     */
    private String gyfe;
    /**
     * 法定代表人
     */
    private String fddbr;
    /**
     * 合同编号
     */
    private String htbh;
}
