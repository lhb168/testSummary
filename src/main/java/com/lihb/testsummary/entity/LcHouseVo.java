package com.lihb.testsummary.entity;

import lombok.Data;

/**
 * @Author lihb
 * @Date 2021/06/18-12:10
 * @Description 聊城返回结果->合同信息
 **/
@Data
public class LcHouseVo {
    /**
     * 不动产单元号
     */
    private String bdcdyh;
    /**
     * 房屋编码
     */
    private String fwbm;
    /**
     * 小区
     */
    private String xq;
    /**
     * 楼号
     */
    private String lh;
    /**
     * 房屋坐落
     */
    private String fwzl;
    /**
     * 建筑结构
     */
    private String jzjgbm;
    /**
     * 户型居室
     */
    private String hxjsbm;
    /**
     * 户型结构
     */
    private String hxjgbm;
    /**
     * 房屋用途
     */
    private String fwytbm;
    /**
     * 房屋类型
     */
    private String fwlxbm;
    /**
     * 房屋性质
     */
    private String Fwxzbm;
    /**
     * 建筑面积
     */
    private String jzmj;
    /**
     * 套内建筑面积
     */
    private String tnjzmj;
    /**
     * 公摊建筑面积
     */
    private String gtjzmj;






}
