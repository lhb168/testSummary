package com.lihb.testsummary.entity;

import java.io.Serializable;

/**
 * 文档内容(Content)实体类
 *
 * @author 李海滨
 * @since 2021-04-30 17:26:36
 */
public class Content implements Serializable {
    private static final long serialVersionUID = -91713446163010437L;
    /**
     * 文档id
     */
    private Long id;
    /**
     * 内容
     */
    private Object content;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }

}
