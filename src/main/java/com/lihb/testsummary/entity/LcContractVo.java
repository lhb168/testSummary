package com.lihb.testsummary.entity;

import lombok.Data;

import java.util.Date;

/**
 * @Author lihb
 * @Date 2021/06/18-12:10
 * @Description 聊城返回结果->合同信息
 **/
@Data
public class LcContractVo {
    /**
     * 合同号/交易证明号
     */
    private String jyywzmzsh;
    /**
     * 交易状态
     */
    private String jyzt;
    /**
     * 交易者全称
     */
    private String jyzqc;
    /**
     * 业务办结时间日期
     */
    private Date ywbjsj;
    /**
     * 法人代表/房屋坐落
     */
    private String fwzl;
    /**
     * 成交金额
     */
    private String cjje;
    /**
     * 合同面积/建筑面积
     */
    private String jzmj;
    /**
     * 合同生效日期
     */
    private Date htsxrq;
    /**
     * 共有方式
     */
    private String gyfsbm;

}
