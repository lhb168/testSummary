package com.lihb.testsummary.entity;

import lombok.Data;

/**
 * 舟山查询危房信息返回VO
 *
 * @Author lihb
 * @Date 2021/8/17-11:45
 * @Description
 **/
@Data
public class ZsWeiFangVo {
    /**
     * 房屋坐落
     */
    private String HOUSE_ADDRESS;
    /**
     * 产权人
     */
    private String REALTYOWNER_NAME;
    /**
     * 房产证号
     */
    private String REALTYCERT_CODE;
    /**
     * 危房登记
     */
    private String WFDJ;
}
