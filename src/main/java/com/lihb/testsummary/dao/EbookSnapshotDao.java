package com.lihb.testsummary.dao;

import com.lihb.testsummary.entity.EbookSnapshot;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 电子书快照表(EbookSnapshot)表数据库访问层
 *
 * @author 李海滨
 * @since 2021-04-30 17:27:07
 */
public interface EbookSnapshotDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    EbookSnapshot queryById(Long id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<EbookSnapshot> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param ebookSnapshot 实例对象
     * @return 对象列表
     */
    List<EbookSnapshot> queryAll(EbookSnapshot ebookSnapshot);

    /**
     * 新增数据
     *
     * @param ebookSnapshot 实例对象
     * @return 影响行数
     */
    int insert(EbookSnapshot ebookSnapshot);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<EbookSnapshot> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<EbookSnapshot> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<EbookSnapshot> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<EbookSnapshot> entities);

    /**
     * 修改数据
     *
     * @param ebookSnapshot 实例对象
     * @return 影响行数
     */
    int update(EbookSnapshot ebookSnapshot);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Long id);

}

